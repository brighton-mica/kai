add_subdirectory(json)
add_subdirectory(SPIRV-Cross)

set(IMGUI_CORE_FILES
    ${CMAKE_HOME_DIRECTORY}/external/imgui/imgui_demo.cpp
    ${CMAKE_HOME_DIRECTORY}/external/imgui/imgui_draw.cpp
    ${CMAKE_HOME_DIRECTORY}/external/imgui/imgui_tables.cpp
    ${CMAKE_HOME_DIRECTORY}/external/imgui/imgui_widgets.cpp
    ${CMAKE_HOME_DIRECTORY}/external/imgui/imgui.cpp
)
set(IMGUI_BACKEND_FILES
    ${CMAKE_HOME_DIRECTORY}/external/imgui/backends/imgui_impl_glfw.cpp
    ${CMAKE_HOME_DIRECTORY}/external/imgui/backends/imgui_impl_vulkan.cpp)

set(IMGUI_SOURCES ${IMGUI_CORE_FILES} ${IMGUI_BACKEND_FILES} PARENT_SCOPE)