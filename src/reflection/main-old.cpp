#include <vulkan/vulkan.h>

#include <vector>
#include <iostream>
#include <fstream>

#include "spirv_glsl.hpp"
#include "nlohmann/json.hpp"

#define EXIT( msg ) do { \
    std::cerr << msg << '\n'; \
    exit( EXIT_FAILURE ); \
} while ( false ) \

std::vector<uint32_t> loadSpirv(std::string filename)
{
    std::ifstream ifs { filename, std::ios::in };

    if (ifs.is_open())
    {
        ifs.seekg(0, ifs.end);
        int nbytes = ifs.tellg();
        ifs.seekg(0, ifs.beg);

        std::vector<uint32_t> buffer;
        buffer.resize(nbytes / sizeof(uint32_t));

        ifs.read(reinterpret_cast<char*>(buffer.data()), nbytes);

        ifs.close();

        return std::move(buffer);
    }
    else
        EXIT("Failed to open file " + filename);
}

namespace json
{
    struct Shader
    {
        std::string name;
        VkShaderStageFlags stage;
        std::string entryPoint;

    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Shader, name, stage, entryPoint);

    struct DescSetLayoutBinding
    {
        uint32_t binding;
        VkDescriptorType descriptorType;
        uint32_t descriptorCount;
        VkShaderStageFlags stageFlags;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(DescSetLayoutBinding, binding, descriptorType, descriptorCount, stageFlags); 

    struct Pipeline
    {
        std::string name;
        std::vector<Shader> shaders;
        std::vector<std::vector<DescSetLayoutBinding>> descSetLayouts;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Pipeline, name, shaders, descSetLayouts);
};

VkShaderStageFlags getShaderStageFromName(const std::string name)
{
    if (name.ends_with("vert"))
        return VK_SHADER_STAGE_VERTEX_BIT;
    if (name.ends_with("geom"))
        return VK_SHADER_STAGE_GEOMETRY_BIT;
    if (name.ends_with("frag")) 
        return VK_SHADER_STAGE_FRAGMENT_BIT;
    if (name.ends_with("comp"))
        return VK_SHADER_STAGE_COMPUTE_BIT;

    EXIT("Unable to deduce shader stage from name " + name);
}

int main(int argc, char const *argv[])
{
    assert(argc == 5);
    std::string workspacePath = argv[1];
    std::string pipelineFile = argv[2];
    std::string glslPath = argv[3];
    std::string spirvPath = argv[4];
    // pipelineFile = workspacePath + pipelineFile;
    glslPath = workspacePath + glslPath;
    spirvPath = workspacePath + spirvPath;

    std::vector<json::Pipeline> pipelines;

    std::ifstream ifs { pipelineFile, std::ios::in };

    if (ifs.is_open())
    {
        nlohmann::json file;
        ifs >> file;
        ifs.close();

        for (auto& pipelineJSON: file["pipelines"])
        {
            json::Pipeline pipeline;
            pipeline.name = pipelineJSON["name"].get<std::string>();

            std::vector<json::Shader> shaders;
            std::map<uint32_t, std::vector<json::DescSetLayoutBinding>> layouts;

            for (auto& shaderJSON : pipelineJSON["shaders"])
            {
                // Compile glsl -> spirv
                std::string name = shaderJSON.get<std::string>();
                std::string command = 
                    "${VULKAN_SDK}/bin/glslangValidator -V -o " + 
                    spirvPath + name + ".spv " +
                    glslPath + name;
                system(command.c_str());

                // Reflect
                std::vector<uint32_t> spirvBinary = loadSpirv(spirvPath + name + ".spv");
                spirv_cross::CompilerGLSL glsl { std::move(spirvBinary) };
                spirv_cross::ShaderResources resources = glsl.get_shader_resources();
                spirv_cross::SmallVector<spirv_cross::EntryPoint> entryPoints = glsl.get_entry_points_and_stages(); // query
                
                json::Shader shader {};
                shader.name = name + ".spv";
                shader.stage = getShaderStageFromName(name);
                shader.entryPoint = entryPoints[0].name;
                shaders.push_back(std::move(shader));

                for (spirv_cross::Resource& ubo_resource : resources.uniform_buffers)
                {
                    auto& ubo = glsl.get_type(ubo_resource.base_type_id);
                    unsigned set = glsl.get_decoration(ubo_resource.id, spv::DecorationDescriptorSet);

                    json::DescSetLayoutBinding descSetLayoutBinding {};
                    descSetLayoutBinding.binding = glsl.get_decoration(ubo_resource.id, spv::DecorationBinding); 
                    descSetLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
                    descSetLayoutBinding.descriptorCount = 1u;
                    descSetLayoutBinding.stageFlags = shader.stage;

                    layouts[set].push_back(std::move(descSetLayoutBinding));
                }
            }

            for (auto& [setIdx, bindings] : layouts)
            {
                pipeline.descSetLayouts.push_back(bindings);
            }

            pipeline.shaders = shaders;
            pipelines.push_back(std::move(pipeline));
        }
    }
    else
        EXIT("Failed to open pipeline config file " + pipelineFile);

    // Create JSON
    nlohmann::json out;
    out["pipelines"] = pipelines;

    // Ouput
    {
        std::string filename = workspacePath + "PipelineReflection.json";
        std::ofstream ofs { filename, std::ios::out };

        if (ofs.is_open())
        {
            std::string output = out.dump(3);
            ofs.write(output.c_str(), output.size());
            ofs.close();
        }
        else
        {
            std::cerr << "Failed to open file! : " << filename << '\n';
            exit(EXIT_FAILURE);
        }
    }
    return 0;
}