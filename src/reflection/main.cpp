#include <vulkan/vulkan.h>
#include <vector>
#include <map>
#include <fstream>
#include <iostream>
#include <string>
#include <memory>
#include <set>


#include "spirv_glsl.hpp"
#include "nlohmann/json.hpp"

#define EXIT( msg ) do { \
    std::cerr << msg << '\n'; \
    exit( EXIT_FAILURE ); \
} while ( false ) \

typedef uint32_t u32;
typedef uint64_t u64;
typedef unsigned char uchar;

std::vector<uint32_t> loadSpirv(std::string filename)
{
    std::ifstream ifs { filename, std::ios::in };

    if (ifs.is_open())
    {
        ifs.seekg(0, ifs.end);
        int nbytes = ifs.tellg();
        ifs.seekg(0, ifs.beg);

        std::vector<uint32_t> buffer;
        buffer.resize(nbytes / sizeof(uint32_t));

        ifs.read(reinterpret_cast<char*>(buffer.data()), nbytes);

        ifs.close();

        return std::move(buffer);
    }
    else
        EXIT("Failed to open file " + filename);
}

nlohmann::json getJsonFromFile(const std::string& filename)
{
    // Read base-materials file
    std::ifstream ifs { filename, std::ios::in };
    nlohmann::json file;

    if (ifs.is_open())
    {
        ifs >> file;
        ifs.close();
    }
    else
        EXIT("Error: failed to open file " + filename);

    //! FIXME - copy created upon return
    return file;
}

namespace json
{
    struct Shader
    {
        std::string name;
        VkShaderStageFlags stage;
        std::string entryPoint;

    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Shader, name, stage, entryPoint);

    struct UBOMember
    {
        u64 offset;
        u64 size;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(UBOMember, offset, size);

    struct UBO
    {
        u64 size;
        std::map<std::string, UBOMember> members;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(UBO, size, members);

    struct DescSetLayoutBinding
    {
        uint32_t binding;
        VkDescriptorType descriptorType;
        uint32_t descriptorCount;
        VkShaderStageFlags stageFlags;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(DescSetLayoutBinding, binding, descriptorType, descriptorCount, stageFlags); 

    struct Pipeline
    {
        std::vector<Shader> shaders;
        std::vector<std::vector<DescSetLayoutBinding>> descSetLayouts;
        UBO ubo;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Pipeline, shaders, descSetLayouts, ubo);
};

VkShaderStageFlags getShaderStageFromName(const std::string name)
{
    if (name.ends_with("vert"))
        return VK_SHADER_STAGE_VERTEX_BIT;
    if (name.ends_with("geom"))
        return VK_SHADER_STAGE_GEOMETRY_BIT;
    if (name.ends_with("frag")) 
        return VK_SHADER_STAGE_FRAGMENT_BIT;
    if (name.ends_with("comp"))
        return VK_SHADER_STAGE_COMPUTE_BIT;

    EXIT("Unable to deduce shader stage from name " + name);
}

int main(int argc, char const* argv[])
{
    assert(argc == 5);
    std::string workspacePath = argv[1];
    std::string configFilePath = argv[2];
    std::string glslPath = argv[3];
    std::string spirvPath = argv[4];

    configFilePath = workspacePath + configFilePath;
    glslPath = workspacePath + glslPath;
    spirvPath = workspacePath + spirvPath;

    const std::string baseMatFilePath = configFilePath + "base-materials.json";
    const std::string instMatFilePath = configFilePath + "material-instances.json";
    const std::string objectsFilePath = configFilePath + "objects.json";

    nlohmann::json baseMatFile = getJsonFromFile(baseMatFilePath);
    nlohmann::json instMatFile = getJsonFromFile(instMatFilePath);

    // Get Num Base Materials
    // Assume worst case and allocate as if every base-material is in use
    const u32 numInstMats = baseMatFile["count"].get<u32>();
    std::unordered_set<u32> baseMatIds;
    std::unordered_map<u32, u32> instMatToBaseMatLookup;
    std::unordered_map<u32, u32> numMatInstRefsLookup;

    // Iter over instance-materials to see all used base-materials
    // A base-material is used if its baseMatPresent[idx] == true
    for (nlohmann::json& instMatJson : instMatFile["material-instances"])
    {
        baseMatIds.insert(instMatJson["base-material-id"].get<u32>());
        instMatToBaseMatLookup[instMatJson["id"].get<u32>()] = instMatJson["base-material-id"].get<u32>();
        numMatInstRefsLookup[instMatJson["base-material-id"].get<u32>()]++;
    }

    // Iter over all active unique base-materials and add their shaders to lookup (to be processed later)
    // Key = json string of shaders, Value = vector of shaders
    u32 shaderSetIdx = 0;
    std::unordered_map<std::string, std::pair<u32, std::vector<std::string>>> shaderConfigs;
    for (const u32 baseMatIdx  : baseMatIds)
    {
        nlohmann::json& shadersJson = baseMatFile["base-materials"][baseMatIdx]["shaders"];
        shaderConfigs[shadersJson.dump()] = std::make_pair(shaderSetIdx++, shadersJson.get<std::vector<std::string>>());
    }

    std::vector<json::Pipeline> pipelines;

    // Iter over all unique shader combinations and reflect
    for (const auto& [key, value]: shaderConfigs)
    {
        json::Pipeline pipeline;

        std::vector<json::Shader> shaders;
        std::map<uint32_t, std::vector<json::DescSetLayoutBinding>> layouts;

        json::UBO materialUBO;

        for (const std::string& shaderName : value.second) 
        {
            // Compile glsl -> spirv
            std::string command = 
                "${VULKAN_SDK}/bin/glslangValidator -V -o " + 
                spirvPath + shaderName + ".spv " +
                glslPath + shaderName;
            system(command.c_str());

            // Reflect
            std::vector<uint32_t> spirvBinary = loadSpirv(spirvPath + shaderName + ".spv");
            spirv_cross::CompilerGLSL glsl { std::move(spirvBinary) };
            spirv_cross::ShaderResources resources = glsl.get_shader_resources();
            spirv_cross::SmallVector<spirv_cross::EntryPoint> entryPoints = glsl.get_entry_points_and_stages(); // query
            
            json::Shader shader {};
            shader.name = shaderName + ".spv";
            shader.stage = getShaderStageFromName(shaderName);
            shader.entryPoint = entryPoints[0].name;
            shaders.push_back(std::move(shader));

            for (spirv_cross::Resource& ubo_resource : resources.uniform_buffers)
            {
                auto& ubo = glsl.get_type(ubo_resource.base_type_id);
                unsigned set = glsl.get_decoration(ubo_resource.id, spv::DecorationDescriptorSet);

                json::DescSetLayoutBinding descSetLayoutBinding {};
                descSetLayoutBinding.binding = glsl.get_decoration(ubo_resource.id, spv::DecorationBinding); 
                descSetLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
                descSetLayoutBinding.descriptorCount = 1u;
                descSetLayoutBinding.stageFlags = shader.stage;

                layouts[set].push_back(std::move(descSetLayoutBinding));

                if (ubo_resource.name == "MaterialUBO")
                {
                    materialUBO.size = glsl.get_declared_struct_size(ubo);
                    const u32 memberCount = ubo.member_types.size();
                    for (u32 memberIdx = 0; memberIdx < memberCount; memberIdx++)
                    {
                        auto& memberType = glsl.get_type(ubo.member_types[memberIdx]);

                        json::UBOMember uboMember {};
                        uboMember.offset = glsl.type_struct_member_offset(ubo, memberIdx);
                        uboMember.size = glsl.get_declared_struct_member_size(ubo, memberIdx);

                        materialUBO.members[glsl.get_member_name(ubo.self, memberIdx)] = std::move(uboMember);
                    }
                }
            }
        }

        for (auto& [setIdx, bindings] : layouts)
        {
            pipeline.descSetLayouts.push_back(bindings);
        }

        pipeline.shaders = shaders;
        pipeline.ubo = std::move(materialUBO);
        pipelines.push_back(std::move(pipeline));
    }

    // Output "shader-set-reflection.json"
    {
        nlohmann::json out;
        out["base-material-shader-sets"] = pipelines;
        std::string filename = configFilePath + "app-state/reflection-base-material-shader-sets.json";
        std::ofstream ofs { filename, std::ios::out };

        if (ofs.is_open())
        {
            std::string output = out.dump(3);
            ofs.write(output.c_str(), output.size());
            ofs.close();
        }
        else
        {
            std::cerr << "Failed to open file! : " << filename << '\n';
            exit(EXIT_FAILURE);
        }
    }

    // Output "reflection-base-materials.json"
    {
        // Add shader-set idx and material-instance count (used to alloc descriptor sets in batch)

        std::vector<nlohmann::json> baseMateralReflections;
        for (const u32 baseMatIdx  : baseMatIds)
        {
            nlohmann::json& baseMatJson = baseMatFile["base-materials"][baseMatIdx];
            baseMatJson["shader-set-idx"] = shaderConfigs[baseMatJson["shaders"].dump()].first;
            baseMatJson["material-instance-ref-count"] = numMatInstRefsLookup[baseMatIdx];;

            auto iter = baseMatJson.find("shaders");
            if (iter == baseMatJson.end())
                EXIT("Error: 'shaders' entry does not exist in base-material!");

            baseMatJson.erase(iter);

            baseMateralReflections.push_back( std::move(baseMatFile["base-materials"][baseMatIdx]));
        }


        std::string filename = configFilePath + "app-state/reflection-base-materials.json";
        std::ofstream ofs { filename, std::ios::out };

        if (ofs.is_open())
        {
            nlohmann::json finalOutput;
            finalOutput["count"] = baseMateralReflections.size();
            finalOutput["base-material-reflections"] = baseMateralReflections;

            std::string output = finalOutput.dump(3);
            ofs.write(output.c_str(), output.size());
            ofs.close();
        }
        else
        {
            std::cerr << "Failed to open file! : " << filename << '\n';
            exit(EXIT_FAILURE);
        }
    }

    // Output "reflection-objects.json"
    {
        // The only difference between "objects.json" and "reflection-objects.json" is that 
        // in the latter objects will have a reference to thier base-material in addition to
        // their material instance. 
        // We do this extra processing here so that when an object is loaded at runtime, it can
        // be immediately placed into its correct render bin (base-material)

        nlohmann::json objectsJson = getJsonFromFile(objectsFilePath);

        for (nlohmann::json& objectJson : objectsJson["objects"])
        {
            objectJson["base-material-id"] = instMatToBaseMatLookup[objectJson["material-instance-id"]];
        }

        std::string filename = configFilePath + "app-state/reflection-objects.json";
        std::ofstream ofs { filename, std::ios::out };

        if (ofs.is_open())
        {
            std::string output = objectsJson.dump(3);
            ofs.write(output.c_str(), output.size());
            ofs.close();
        }
        else
        {
            std::cerr << "Failed to open file! : " << filename << '\n';
            exit(EXIT_FAILURE);
        }
    }

    return 0;
}