#version 450

// in
layout(location = 1) in vec2 inUV;
layout(location = 0) in vec3 inPosition;



layout(set = 0, binding = 0) uniform UBO1
{
    float ubo1;
};

layout(set = 0, binding = 1) uniform UBO2
{
    float ubo2;
};

layout(set = 0, binding = 3) uniform UBO3
{
    float ubo3;
};


layout(set = 1, binding = 0) uniform ObjectUBO
{
   mat4 modelMatrix; 
   vec2 array;
};

void main()
{
    gl_Position = vec4(inPosition, 1.0f);
}