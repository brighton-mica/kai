#include <vulkan/vulkan.h>
#include <iostream>
#include <fstream>
#include <vector>

#include "nlohmann/json.hpp"

// struct Format
// {
//     struct DescSetLayoutBinding
//     {
//         uint32_t binding;
//         VkDescriptorType type;
//         uint32_t descriptorCount;
//         VkShaderStageFlags stageFlags;
//     };

//     struct DescSetLayout
//     {
//         uint32_t bindingCount;
//         std::vector<uint32_t> bindingIdxs;
//     };

//     std::vector<DescSetLayoutBinding> bindings;
//     std::vector<DescSetLayout> layouts;
// };

// void to_json(nlohmann::json& j, const Format::DescSetLayoutBinding& obj)
// {
//     j = nlohmann::json {
//         {"binding", obj.binding},
//         {"type", obj.type},
//         {"descriptorCount", obj.descriptorCount},
//         {"stageFlags", obj.stageFlags},
//     };
// }

// void from_json(const nlohmann::json& j, Format::DescSetLayoutBinding& obj)
// {
//     j.at("binding").get_to(obj.binding);
//     j.at("type").get_to(obj.type);
//     j.at("descriptorCount").get_to(obj.descriptorCount);
//     j.at("stageFlags").get_to(obj.stageFlags);
// }


// void to_json(nlohmann::json& j, const Format::DescSetLayout& obj)
// {
//     j = nlohmann::json {
//         {"bindingCount", obj.bindingCount},
//         {"bindingIdxs", obj.bindingIdxs},
//     };
// }

// void from_json(const nlohmann::json& j, Format::DescSetLayout& obj)
// {
//     j.at("bindingCount").get_to(obj.bindingCount);
//     j.at("bindingIdsx").get_to(obj.bindingIdxs);
// }


// class Reflection
// {
// private:
//     VkDevice device;

//     std::vector<VkDescriptorSetLayoutBinding> m_vkDescSetLayoutBindings; // unique set of bindings
//     std::vector<VkDescriptorSetLayout> m_vkDescSetLayouts; // unique set of desc set layouts
//     std::vector<VkPipelineLayout> pipelineLayouts; // unique set of pipeline layouts
// public:
//     Reflection()
//     {
//         // Read Pipelines.json
//         std::ifstream ifs { "pipelineLayout.json", std::ios::in };

//         if (ifs.is_open())
//         {
//             nlohmann::json data;
//             ifs >> data;

//             std::cout << data.dump(3) << '\n';

//             for (const Format::DescSetLayoutBinding& descSetLayoutBinding : data["VkDescriptorSetLayoutBindings"])
//             {
//                 VkDescriptorSetLayoutBinding vkDescSetLayoutBindning = {};
//                 vkDescSetLayoutBindning.binding = descSetLayoutBinding.binding;
//                 vkDescSetLayoutBindning.descriptorType = descSetLayoutBinding.type;
//                 vkDescSetLayoutBindning.descriptorCount = descSetLayoutBinding.descriptorCount;
//                 vkDescSetLayoutBindning.stageFlags = descSetLayoutBinding.stageFlags;

//                 m_vkDescSetLayoutBindings.push_back(std::move(vkDescSetLayoutBindning));
//             }

//             for (const Format::DescSetLayout& descSetLayout : data["VkDescriptorSetLayout"])
//             {
//                 std::vector<VkDescriptorSetLayoutBinding&> bindings;
//                 bindings.reserve(descSetLayout.bindingCount);
//                 for (uint32_t i = 0; i < descSetLayout.bindingCount; ++i)
//                     bindings.push_back(m_vkDescSetLayoutBindings[descSetLayout.bindingIdxs[i]]);

//                 VkDescriptorSetLayoutCreateInfo createInfo = {};
//                 createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
//                 createInfo.bindingCount = descSetLayout.bindingCount;
//                 createInfo.pBindings = bindings.data();

//                 VkDescriptorSetLayout layout;
//                 vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &layout);

//                 m_vkDescSetLayouts.push_back(layout);
//             }

//             ifs.close();
//         }
//         else
//         {
//             std::cerr << "ERROR: pipelineLayout.json does not exist\n";
//             exit(EXIT_FAILURE);
//         }
//     }
// };

#define VK_CHECK( val ) do {         \
    if ( val != VK_SUCCESS )         \
    {                                \
        assert( val == VK_SUCCESS ); \
    }                                \
} while( false )                     \


#define EXIT( msg ) do { \
    std::cerr << msg << '\n'; \
    exit( EXIT_FAILURE ); \
} while ( false ) \

#include <array>

namespace json
{
    
};

enum ShaderType
{
    eVertex = 0,
    eGeometry = 1,
    eFragment = 2,
    eCompute = 4,
    eNumShaderTypes
};

enum PipelineType
{
    eDefault = 0,
    eLightSrc = 1,
};

PipelineType getPipelineTypeFromName(const std::string& name)
{
    static std::unordered_map<std::string, PipelineType> mapping {
        { "default", eDefault }
    };

    auto itr = mapping.find(name);
    if (itr != mapping.end())
        return itr->second;

    EXIT("Invalid pipeline name!\n");
}

class Pipeline
{
private:
    static VkDevice device;


    static void createDefaultPipeline(VkGraphicsPipelineCreateInfo& createInfo)
    {
        std::cout << "createDefaultPipeline()\n";
    }

    static void createLightSrcPipeline(VkGraphicsPipelineCreateInfo& createInfo)
    {
        std::cout << "createLightSrcPipeline()\n";
    }

    static VkShaderModule createShaderModule(const std::string& filename)
    {
        std::ifstream ifs { filename, std::ios::in };

        if (ifs.is_open())
        {
            ifs.seekg(0, ifs.end);
            int nbytes = ifs.tellg();
            ifs.seekg(0, ifs.beg);

            std::vector<uint32_t> buffer;
            buffer.resize(nbytes / sizeof(uint32_t));

            ifs.read(reinterpret_cast<char*>(buffer.data()), nbytes);

            ifs.close();

            VkShaderModuleCreateInfo createInfo {};
            createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
            createInfo.codeSize = nbytes;
            createInfo.pCode = buffer.data();

            VkShaderModule shaderModule;
            // VK_CHECK(vkCreateShaderModule(device, &createInfo, NULL, &shaderModule));

            return shaderModule;
        }
        else
            EXIT("Failed to open file");
    }

    VkPipelineLayout m_vkPipelineLayout;
    VkPipeline m_vkPipeline;
public:
    // Pipeline(std::string name, std::array<std::string, eNumShaderTypes> shaderNames)
    Pipeline(const nlohmann::json& json)
    {
        std::cout << "PIPELINE()\n";
        // Create shader stages
        std::vector<VkPipelineShaderStageCreateInfo> shaderStages;

        for (uint8_t idx = 0; idx < eNumShaderTypes; ++idx)
        for (const auto& shaderStageInfo : json["shaders"])
        {
            if (shaderStageInfo["name"].empty())
                continue;
            
            VkPipelineShaderStageCreateInfo createInfo {};
            createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
            createInfo.stage = shaderStageInfo["stage"].get<VkShaderStageFlagBits>();
            // createInfo.module = createShaderModule(shaderStageInfo["name"].get<std::string>());
            createInfo.pName = shaderStageInfo["entryPoint"].get<std::string>().c_str();

            shaderStages.push_back(std::move(createInfo));
        }

        // Create pipeline layout
        std::vector<VkDescriptorSetLayout> descSetLayouts;

        for (const auto& layoutInfo : json["VkDescriptorSetLayouts"])
        {
            std::vector<VkDescriptorSetLayoutBinding> descSetLayoutBindings;

            for (const auto& bindingInfo : layoutInfo["pBindings"])
            {
                VkDescriptorSetLayoutBinding layoutBinding {};
                layoutBinding.binding = bindingInfo["binding"].get<uint32_t>();
                layoutBinding.descriptorType = bindingInfo["descriptorType"].get<VkDescriptorType>();
                layoutBinding.descriptorCount = bindingInfo["descriptorCount"].get<uint32_t>();
                layoutBinding.stageFlags = bindingInfo["stageFlags"].get<VkShaderStageFlags>();

                descSetLayoutBindings.push_back(std::move(layoutBinding));
            }

            VkDescriptorSetLayoutCreateInfo createInfo {};
            createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
            createInfo.bindingCount = static_cast<uint32_t>(descSetLayoutBindings.size()); 
            createInfo.pBindings = descSetLayoutBindings.data();

            VkDescriptorSetLayout layout;
            // VK_CHECK(vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &layout));

            descSetLayouts.push_back(layout);
        }

        VkPipelineLayoutCreateInfo layoutCreateInfo {};
        layoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        layoutCreateInfo.setLayoutCount = static_cast<uint32_t>(descSetLayouts.size());
        layoutCreateInfo.pSetLayouts = descSetLayouts.data();
        layoutCreateInfo.pushConstantRangeCount = 0u;
        layoutCreateInfo.pPushConstantRanges = nullptr;

        // VK_CHECK(vkCreatePipelineLayout(device, &layoutCreateInfo, nullptr, &m_vkPipelineLayout));

        VkGraphicsPipelineCreateInfo pipelineCreateInfo {};
        pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        pipelineCreateInfo.stageCount = static_cast<uint32_t>(shaderStages.size());
        pipelineCreateInfo.pStages = shaderStages.data();
        pipelineCreateInfo.layout = m_vkPipelineLayout;

        PipelineType type = getPipelineTypeFromName(json["name"].get<std::string>());

        switch (type)
        {
            case eDefault:
                createDefaultPipeline(pipelineCreateInfo);
                break;
            case eLightSrc:
                createLightSrcPipeline(pipelineCreateInfo);
                break;
            default:
                EXIT("Invalid pipeline type!");
                break;
        }

        // VK_CHECK(vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1u, &pipelineCreateInfo, nullptr, &m_vkPipeline));

        // for (VkPipelineShaderStageCreateInfo& stage : shaderStages)
        //     vkDestroyShaderModule(device, stage.module, nullptr);
    }
};

#include <memory>

int main()
{

    /*
    jsonData = json.read()
    for jsonPipeline in jsonData 
        Pipeline pipeline { jsonPipeline.name, jsonPipeline.shaders }

        pipelines[jsonPipeline.name] = std::move(pipeline)
    */

   std::ifstream ifs { "../test/PipelinesConfig.json", std::ios::in };

   if (ifs.is_open())
   {
       nlohmann::json file;
       ifs >> file;
       ifs.close();

       std::vector<std::unique_ptr<Pipeline>> pipelines;

       for (auto& element : file["pipelines"])
       {
        //    Pipeline pipelines { element };
        //    std::unique_ptr<Pipeline> pipeline = std::make_unique<Pipeline>(element);
        //    pipelines.push_back(std::move(pipeline));
           pipelines.emplace_back(element);
       }
   }
   else
       EXIT("Failed to open pipeline config file");

    return 0;
}