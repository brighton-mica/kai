#include "SPIRV-Cross/spirv_glsl.hpp"
#include "nlohmann/json.hpp"


#include <vulkan/vulkan.h>
#include <vector>
#include <utility>
#include <iterator>

#include <iostream>
#include <fstream>

static std::string g_pathFromExecutable = "../SPIRV/";
static std::string g_currentPipeline;
static std::map<std::string, std::vector<std::string>> g_pipelineMap;

std::vector<uint32_t> load_spirv(const char* filename)
{
    std::ifstream ifs { filename, std::ios::in };

    if (ifs.is_open())
    {
        ifs.seekg(0, ifs.end);
        int nbytes = ifs.tellg();
        ifs.seekg(0, ifs.beg);

        std::vector<uint32_t> buffer;
        buffer.resize(nbytes / sizeof(uint32_t));

        ifs.read(reinterpret_cast<char*>(buffer.data()), nbytes);

        ifs.close();

        return std::move(buffer);
    }
    else
    {
        std::cout << "Failed to open file!\n";
        exit(EXIT_FAILURE);
    }
}

struct UniformBuffer
{
    std::string m_name;
    size_t m_size;

    unsigned m_set;
    unsigned m_binding;

    struct Member
    {
        std::string m_name;
        size_t m_offset;
        size_t m_size;
    };

    std::vector<Member> members;
};

struct InputBinding
{
    size_t location;
    size_t stride;
    VkFormat format;
};

struct InputAttributes
{
    std::vector<InputBinding> bindings;
};


/**
 * @brief Create a VkDescriptorSetLayoutBinding object
 * 
 * @param binding - corresponds to a resource of the same binding number in the shader stages
 * @param type - type of resource descriptors being used for this binding
 * @param count - the number of descriptors contained in the binding
 * @param stageFlags - which pipeline shader stages can access a resource descriptor from this binding
 * @return VkDescriptorSetLayoutBinding 
 */
VkDescriptorSetLayoutBinding createDescSetLayoutBinding(
    const uint32_t binding,  
    const VkDescriptorType type,
    const uint32_t count, 
    const VkShaderStageFlags stageFlags)
{
    VkDescriptorSetLayoutBinding descSetLayoutBinding {};
    descSetLayoutBinding.binding = binding;
    descSetLayoutBinding.descriptorType = type;
    descSetLayoutBinding.descriptorCount = count;
    descSetLayoutBinding.stageFlags = stageFlags;

    return descSetLayoutBinding;
}


#include<unordered_set>

class HashFunction
{
public:
    size_t operator()(const VkDescriptorSetLayoutBinding& binding)
    {
        // bits [0,7] reserved for set
        // bits [8,23] reserved for binding
        // bits [24,63] reserved for type

        size_t hash = static_cast<size_t>(binding.descriptorType);
        hash |= (hash << 24) | binding.binding;
        hash |= (hash << 8) | binding.stageFlags;

        return hash;
    }
};


struct DescSetLayout
{
    uint32_t bindingCount;
    std::vector<uint32_t> bindingIdxs;
    std::pair<std::string, uint32_t> pipelineNameSetIdxPair;

    DescSetLayout() 
        : bindingCount { 0 }
        , bindingIdxs {}
        , pipelineNameSetIdxPair {}
    {}

    bool operator==(const DescSetLayout& other)
    {
       return this->bindingCount == other.bindingCount &&
        std::equal(this->bindingIdxs.begin(), this->bindingIdxs.end(), other.bindingIdxs.begin());
    }
};

struct DescSetLayoutBinding
{
    DescSetLayoutBinding() = default;
    uint32_t binding;
    VkDescriptorType type;
    uint32_t descriptorCount;
    VkShaderStageFlags stageFlags;

    bool operator==(const DescSetLayoutBinding& other)
    {
        return 
            this->binding == other.binding &&
            this->type == other.type &&
            this->descriptorCount == other.descriptorCount;
    }
};

struct Pipeline
{
    std::string name;
    std::map<uint32_t, uint32_t> layouts;

    uint32_t layoutCount;
    std::vector<uint32_t> layoutIdxs;
    uint32_t pushConstantRangeCount;
    std::vector<uint32_t> pushConstantRangeIdxs;
};

void to_json(nlohmann::json& j, const DescSetLayoutBinding& obj)
{
    j = nlohmann::json {
        {"binding", obj.binding},
        {"type", obj.type},
        {"descriptorCount", obj.descriptorCount},
        {"stageFlags", obj.stageFlags},
    };
}

void from_json(const nlohmann::json& j, DescSetLayoutBinding& obj)
{
    j.at("binding").get_to(obj.binding);
    j.at("type").get_to(obj.type);
    j.at("descriptorCount").get_to(obj.descriptorCount);
    j.at("stageFlags").get_to(obj.stageFlags);
}


void to_json(nlohmann::json& j, const DescSetLayout& obj)
{
    j = nlohmann::json {
        {"bindingCount", obj.bindingCount},
        {"bindingIdxs", obj.bindingIdxs},
    };
}

void from_json(const nlohmann::json& j, DescSetLayout& obj)
{
    j.at("bindingCount").get_to(obj.bindingCount);
    j.at("bindingIdsx").get_to(obj.bindingIdxs);
}

void to_json(nlohmann::json& j, const Pipeline& obj)
{
    j = nlohmann::json {
        { obj.name, {
            { "layoutCount", obj.layoutCount },
            { "layoutIdxs", obj.layoutIdxs }
        }}
            // { "layoutIdxs", obj.layouts },
    };
}

void from_json(const nlohmann::json& j, Pipeline& obj)
{
    j.at("name").get_to(obj.name);
    j.at("layoutCount").get_to(obj.layoutCount);
    j.at("layoutIdxs").get_to(obj.layoutIdxs);
}

static std::vector<DescSetLayoutBinding> globalDescSetLayoutBindings;  
static std::vector<DescSetLayout> globalDescSetLayouts;
static std::vector<Pipeline> globalPipelines;
static std::map<std::string, Pipeline> g_Pipelines;

static VkShaderStageFlags getShaderStageFromName(std::string filename)
{
    if (filename.starts_with("vert"))
        return VK_SHADER_STAGE_VERTEX_BIT;
    else if (filename.starts_with("geom"))
        return VK_SHADER_STAGE_GEOMETRY_BIT;
    else if (filename.starts_with("frag"))
        return VK_SHADER_STAGE_FRAGMENT_BIT;
    else if (filename.starts_with("comp"))
        return VK_SHADER_STAGE_COMPUTE_BIT;
    else
    {
        std::cerr << "Specified shader file (" << filename << ") is not recognized.\n";
        exit(EXIT_FAILURE);
    }
}

static void processSPVFile(std::string filename, std::vector<DescSetLayoutBinding>& bindings)
{
    const VkShaderStageFlags stageFlag = getShaderStageFromName(filename);
    const std::string fullPath = g_pathFromExecutable + filename;
    const std::vector<uint32_t> spirv_binary = load_spirv(fullPath.c_str());
	spirv_cross::CompilerGLSL glsl { std::move(spirv_binary) };
	spirv_cross::ShaderResources resources = glsl.get_shader_resources();

    for (spirv_cross::Resource& ubo_resource : resources.uniform_buffers)
    {
        auto& ubo = glsl.get_type(ubo_resource.base_type_id);

        DescSetLayoutBinding binding;
        binding.descriptorCount = 1u;
        binding.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        binding.binding = glsl.get_decoration(ubo_resource.id, spv::DecorationBinding);
        binding.stageFlags = stageFlag;

        std::vector<DescSetLayoutBinding>::iterator itr = std::find(
            bindings.begin(),
            bindings.end(),
            binding);

        if (itr != bindings.end())
        {
            // Binding has already been processed, simply OR shader stage flags
            itr->stageFlags |= stageFlag;
        }
        else
        {
            // Binding has not yet been processed, add it to list
            bindings.push_back(std::move(binding));
        }
    }
}

static void processSPVFile(std::string filename, std::unordered_map<uint32_t, DescSetLayout>& layouts)
{
    static auto bindingsEqual = [](const DescSetLayoutBinding& a, const DescSetLayoutBinding& b)
    {
        return a.binding == b.binding &&
            a.descriptorCount == b.descriptorCount &&
            a.stageFlags == b.stageFlags && 
            a.type == b.type;
    };

    const VkShaderStageFlags stageFlag = getShaderStageFromName(filename);
    const std::string fullPath = g_pathFromExecutable + filename;
    const std::vector<uint32_t> spirv_binary = load_spirv(fullPath.c_str());
	spirv_cross::CompilerGLSL glsl { std::move(spirv_binary) };
	spirv_cross::ShaderResources resources = glsl.get_shader_resources();

    // Pass - create all DescSetLayouts used in this shader
    for (spirv_cross::Resource& ubo_resource : resources.uniform_buffers)
    {
        auto& ubo = glsl.get_type(ubo_resource.base_type_id);

        DescSetLayoutBinding binding;
        binding.descriptorCount = 1u;
        binding.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        binding.binding = glsl.get_decoration(ubo_resource.id, spv::DecorationBinding);
        binding.stageFlags = stageFlag;

        uint32_t idx = 0;
        for (; idx < globalDescSetLayoutBindings.size(); ++idx)
        {
            if (bindingsEqual(binding, globalDescSetLayoutBindings[idx]))
                break;
        }

        if (idx != globalDescSetLayoutBindings.size())
        {
            // Binding has been found, get idx in global list and store in layout
            unsigned set = glsl.get_decoration(ubo_resource.id, spv::DecorationDescriptorSet);

            layouts[set].bindingCount++;
            layouts[set].bindingIdxs.push_back(idx);
        }
        else
        {
            std::cerr << "Attemping to point a DescSetLayout to a binding which was never processed!\n";
            exit(EXIT_FAILURE);
        }
    }
}

static void processSPVFile(std::string filename, std::vector<Pipeline>& pipelines)
{
    // i was thinking here that i would have to remake binding and set in order to get right set idx for pipeline
    // maybe I coule just do this pass first and add get all the info I need as i go
    for (const DescSetLayout& layout : globalDescSetLayouts)
    {
        if (layout.pipelineNameSetIdxPair.first == filename)
        {

        }
    }

    // const VkShaderStageFlags stageFlag = getShaderStageFromName(filename);
    // const std::string fullPath = g_pathFromExecutable + filename;
    // const std::vector<uint32_t> spirv_binary = load_spirv(fullPath.c_str());
	// spirv_cross::CompilerGLSL glsl { std::move(spirv_binary) };
	// spirv_cross::ShaderResources resources = glsl.get_shader_resources();

    // // Pass - create all DescSetLayouts used in this shader
    // for (spirv_cross::Resource& ubo_resource : resources.uniform_buffers)
    // {
    //     auto& ubo = glsl.get_type(ubo_resource.base_type_id);

    //     DescSetLayoutBinding binding;
    //     binding.descriptorCount = 1u;
    //     binding.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    //     binding.binding = glsl.get_decoration(ubo_resource.id, spv::DecorationBinding);
    //     binding.stageFlags = stageFlag;

    //     std::vector<DescSetLayoutBinding>::iterator itr = std::find(
    //         globalDescSetLayoutBindings.begin(),
    //         globalDescSetLayoutBindings.end(),
    //         binding);

    //     if (itr != globalDescSetLayoutBindings.end())
    //     {
    //         // Binding has been found, get idx in global list and store in layout
    //         unsigned set = glsl.get_decoration(ubo_resource.id, spv::DecorationDescriptorSet);

    //         layouts[set].bindingCount++;
    //         layouts[set].bindingIdxs.push_back(std::distance(globalDescSetLayoutBindings.begin(), itr));
    //     }
    //     else
    //     {
    //         std::cerr << "Attemping to point a DescSetLayout to a binding which was never processed!\n";
    //         exit(EXIT_FAILURE);
    //     }
    // }
}

static void processDescSetLayoutBindings(std::vector<std::string> shaderNames)
{
    static auto bindingsEqual = [](const DescSetLayoutBinding& a, const DescSetLayoutBinding& b)
    {
        return a.binding == b.binding &&
            a.descriptorCount == b.descriptorCount &&
            a.stageFlags == b.stageFlags && 
            a.type == b.type;
    };

    std::vector<DescSetLayoutBinding> pipelineBindings;

    for (const std::string& name : shaderNames)
    {
        processSPVFile(name, pipelineBindings);
    }

    // Add pipelineBindings to global
    for (const DescSetLayoutBinding& binding : pipelineBindings)
    {
        uint32_t idx = 0;
        for (; idx < globalDescSetLayoutBindings.size(); ++idx)
        {
            if (bindingsEqual(binding, globalDescSetLayoutBindings[idx]))
                break;
        }

        if (idx == globalDescSetLayoutBindings.size())
        {
            // Binding with this state has not yet been processed, add it the global list
            globalDescSetLayoutBindings.push_back(std::move(binding));
        }
    }
}

static void processDescSetLayouts(std::vector<std::string> filepaths)
{
    std::unordered_map<uint32_t, DescSetLayout> pipelineLayouts;
    
    for (const std::string& path : filepaths)
    {
        processSPVFile(path, pipelineLayouts);
    }

    g_Pipelines[g_currentPipeline].name = g_currentPipeline;

    // Add layouts to global 
    for (auto& [setIdx, layout] : pipelineLayouts)
    {
        std::vector<DescSetLayout>::iterator itr = std::find(
            globalDescSetLayouts.begin(),
            globalDescSetLayouts.end(),
            layout);
        
        uint32_t globalIdx = 0;
        if (itr == globalDescSetLayouts.end())
        {
            globalIdx = globalDescSetLayouts.size();
            // Layout has not yet been processed, add it to list
            globalDescSetLayouts.push_back(std::move(layout));
        }
        else
        {
            globalIdx = std::distance(globalDescSetLayouts.begin(), itr);
        }

        g_Pipelines[g_currentPipeline].layoutCount++;
        g_Pipelines[g_currentPipeline].layouts[setIdx] = globalIdx;
    }

    for (const auto& [setIdx, layoutIdx] : g_Pipelines[g_currentPipeline].layouts)
    {
        g_Pipelines[g_currentPipeline].layoutIdxs.push_back(layoutIdx);
    }
}

void readPipelinesJSONFile(std::string filename)
{
    std::ifstream ifs { "../Pipelines.json", std::ios::in }; 

    nlohmann::json json_reader;
    if (ifs.is_open())
    {
        ifs >> json_reader;
        ifs.close();

        std::cout << json_reader.dump(3) << '\n';
    }
    else
    {
        std::cerr << "Failed to read Pipelines.json\n";
        exit(EXIT_FAILURE);
    }

    g_pipelineMap = json_reader;
}



int main(int argc, char const *argv[])
{
    assert(argc == 2 && "Incorrect number of arguments supplied!");
    const std::string pipelineFile = argv[1];

    // Read "Pipelines.json"
    readPipelinesJSONFile(pipelineFile);

    // First pass - populate g_descSetLayoutBindings with unique DescSetLayoutBindings
    for (const auto& [pipelineName, shaderNames] : g_pipelineMap)
    {
        processDescSetLayoutBindings(shaderNames);
    }

    // Second pass - 
    for (const auto& [pipelineName, shaderNames] : g_pipelineMap)
    {
        g_currentPipeline = pipelineName;
        processDescSetLayouts(shaderNames);
    }

    // Create JSON
    nlohmann::json out;
    {
        nlohmann::json json_bindings;
        for (const DescSetLayoutBinding& binding : globalDescSetLayoutBindings)
            json_bindings.push_back(binding);

        nlohmann::json json_layouts;
        for (const DescSetLayout& layout : globalDescSetLayouts)
            json_layouts.push_back(layout);

        nlohmann::json json_pipelines;
        for (const auto& [pipelineName, pipeline] : g_Pipelines)
        {
            json_pipelines.push_back(pipeline);
        }

        out["VkDescriptorSetLayoutBindings"] = json_bindings;
        out["VkDescriptorSetLayouts"] = json_layouts;
        // out["Pipelines"] = json_pipelines;
    }

    std::cout << out.dump(1) << "\n";

    // Ouput
    {
        const char* filename = "pipelineLayout.json";
        std::ofstream ofs { filename, std::ios::out };

        if (ofs.is_open())
        {
            std::string output = out.dump(3);
            ofs.write(output.c_str(), output.size());
            ofs.close();
        }
        else
        {
            std::cerr << "Failed to open file! : " << filename << '\n';
            exit(EXIT_FAILURE);
        }
    }

    return 0;
}
