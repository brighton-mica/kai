#version 450

// ins
layout(location = 0) in vec3 inPosition;

// sets
layout(set = 0, binding = 0) uniform FrameUBO
{
    mat4 projMatrix;
    mat4 viewMatrix;
};

layout(set = 1, binding = 0) uniform MaterialUBO
{
    vec3 color; 
};

// outs
layout(location = 0) out vec3 outColor;

void main()
{
    gl_Position = vec4(inPosition, 1.0f);
    outColor = color;
}
