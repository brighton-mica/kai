#ifndef KAI_DEVICE_HPP
#define KAI_DEVICE_HPP

#include <vulkan/vulkan.h>

class GLFWwindow;

namespace kai
{
    class Device
    {
    private:
        VkInstance m_vkInstance;
        VkSurfaceKHR m_vkSurface;
        VkPhysicalDevice m_vkPhysicalDevice;
        VkDevice m_vkDevice;

        VkPhysicalDeviceMemoryProperties m_vkPhysicalDeviceMemoryProps;

        struct Queue
        {
            uint32_t familyIndex;
            VkQueue vkQueue;
        };

        Queue m_grapicsQueue;

    public:
        Device(GLFWwindow* window);
        ~Device();

        VkDevice getDevice() const { return m_vkDevice; }
        VkInstance getInstance() const { return m_vkInstance; }
        VkSurfaceKHR getSurface() const { return m_vkSurface; }
        VkPhysicalDevice getPhysicalDevice() const { return m_vkPhysicalDevice; }
        VkQueue getQueue(VkQueueFlags flag) const;
        uint32_t getQueueFamilyIdx(VkQueueFlags flag) const;
        void waitIdle() const { vkDeviceWaitIdle(m_vkDevice); }
    };
};

#endif // KAI_DEVICE_HPP