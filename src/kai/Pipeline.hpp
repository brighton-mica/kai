#ifndef KAI_PIPELINE_HPP
#define KAI_PIPELINE_HPP

#include "Device.hpp"
#include "Defines.hpp"

#include "nlohmann/json.hpp"

#include <vulkan/vulkan.h>
#include <assert.h>
#include <string>
#include <unordered_map>
#include <vector>
#include <iostream>
#include <fstream>


namespace kai
{
    class Swapchain;

    struct PipelineConfig
    {
        uint32_t stageCount;
        std::vector<VkPipelineShaderStageCreateInfo> shaderStages;
        std::vector<VkVertexInputBindingDescription> vertexInputBindings;
        std::vector<VkVertexInputAttributeDescription> vertexInputAttribtutes;
        std::vector<VkViewport> viewports;
        std::vector<VkRect2D> scissors;
        VkPipelineDepthStencilStateCreateInfo depthStencilState;
        std::vector<VkPipelineColorBlendAttachmentState> colorAttachmentBlendStates;
        VkPipelineColorBlendStateCreateInfo colorBlendState;
        VkPipelineInputAssemblyStateCreateInfo inputAssemblyState;
        VkPipelineRasterizationStateCreateInfo rasterizationState;
        VkPipelineMultisampleStateCreateInfo multisampleState;
        VkPipelineLayout layout;
        VkRenderPass renderPass;
    };

    class Pipeline
    {
    private:
        static Swapchain* m_kSwapchain;

        Device& m_kDevice;

        VkPipelineLayout m_vkPipelineLayout;
        VkPipeline m_vkPipeline;
        std::vector<VkDescriptorSetLayout> m_vkDescriptorSetLayouts;

        std::vector<VkShaderModule> m_vkShaderModules;
        std::vector<VkPipelineShaderStageCreateInfo> shaderStages;
    public:
        static void setSwapchain(Swapchain* swapchain) { m_kSwapchain = swapchain; }

        Pipeline(Device& device);
        ~Pipeline();

        void setupPipeline(const nlohmann::json& json, PipelineConfig& pipelineConfig);
        void createPipeline(PipelineConfig& createInfo);

        VkPipeline getPipeline() const { return m_vkPipeline; }
        VkPipelineLayout getPipelineLayout() const { return m_vkPipelineLayout; }
        const VkDescriptorSetLayout& getDescriptorSetLayout(const uint32_t setIdx)
        { 
            assert(setIdx < m_vkDescriptorSetLayouts.size());
            return m_vkDescriptorSetLayouts[setIdx];
        }
    };

}; // namespace kai

#endif // KAI_PIPELINE_HPP