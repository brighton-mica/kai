#include "Swapchain.hpp"
#include "Pipeline.hpp"
#include "Defines.hpp"

static VkShaderModule createShaderModule(VkDevice device, const std::string& filename)
{
    std::ifstream ifs { "../src/spirv/" + filename, std::ios::in };

    if (ifs.is_open())
    {
        ifs.seekg(0, ifs.end);
        int nbytes = ifs.tellg();
        ifs.seekg(0, ifs.beg);

        std::vector<uint32_t> buffer;
        buffer.resize(nbytes / sizeof(uint32_t));

        ifs.read(reinterpret_cast<char*>(buffer.data()), nbytes);

        ifs.close();

        VkShaderModuleCreateInfo createInfo {};
        createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        createInfo.codeSize = nbytes;
        createInfo.pCode = buffer.data();

        VkShaderModule shaderModule;
        VK_CHECK(vkCreateShaderModule(device, &createInfo, NULL, &shaderModule));

        return shaderModule;
    }
    else
        EXIT("Failed to open file " + filename);
}

namespace kai
{
    Swapchain* Pipeline::m_kSwapchain = nullptr;

    static void defaultPipelineInit(PipelineConfig& pipelineConfig, VkExtent2D swapchainExtent)
    {
        const VkViewport viewport {
            .x = 0,
            .y = 0,
            .width = static_cast<float>(swapchainExtent.width),
            .height = static_cast<float>(swapchainExtent.height),
            .minDepth = 0.0f,
            .maxDepth = 1.0f,
        };

        const VkRect2D scissorRect {
            .offset = {.x = 0, .y = 0},
            .extent = swapchainExtent,
        };

        const VkPipelineDepthStencilStateCreateInfo pipelineDepthStencilStateCreateInfo {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
            .depthTestEnable = VK_TRUE,
            .depthWriteEnable = VK_TRUE,
            .depthCompareOp = VK_COMPARE_OP_LESS,
            .depthBoundsTestEnable = VK_FALSE,
            .stencilTestEnable = VK_FALSE,
            .minDepthBounds = 0.0f,
            .maxDepthBounds = 1.0f };

        const VkPipelineColorBlendAttachmentState pipelineColorBlendState {
            .blendEnable = VK_FALSE,
            .srcColorBlendFactor = VK_BLEND_FACTOR_ZERO,
            .dstColorBlendFactor = VK_BLEND_FACTOR_ZERO,
            .colorBlendOp = VK_BLEND_OP_ADD,
            .srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
            .dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
            .alphaBlendOp = VK_BLEND_OP_ADD,
            .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
        };
        
        const VkPipelineColorBlendStateCreateInfo pipelineColorBlendStateCreateInfo {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
            .logicOpEnable = VK_FALSE,
            .logicOp = VK_LOGIC_OP_COPY,
            .attachmentCount = 1,
            .pAttachments = &pipelineColorBlendState, // will be invalid on function scope exit
            .blendConstants = { 0, 0, 0, 0 }
        };

        const VkPipelineInputAssemblyStateCreateInfo pipelineInputAssemblyStateCreateInfo {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
            .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
            .primitiveRestartEnable = VK_FALSE
        };

        const VkPipelineRasterizationStateCreateInfo pipelineRasterizationStateCreateInfo {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
            .depthClampEnable = VK_FALSE,
            .rasterizerDiscardEnable = VK_FALSE,
            .polygonMode = VK_POLYGON_MODE_FILL,
            .cullMode = VK_CULL_MODE_NONE,
            .frontFace = VK_FRONT_FACE_CLOCKWISE,
            .depthBiasEnable = VK_FALSE,
            .depthBiasConstantFactor = 0.f,
            .depthBiasClamp = 0.f,
            .depthBiasSlopeFactor = 0.f,
            .lineWidth = 1.f,
        };

        const VkPipelineMultisampleStateCreateInfo pipelineMulisampleStateCreateInfo  {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
            .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
            .sampleShadingEnable = VK_FALSE,
            .minSampleShading = 0,
            .pSampleMask = nullptr,
            .alphaToCoverageEnable = VK_FALSE,
            .alphaToOneEnable = VK_FALSE,
        };

        pipelineConfig.viewports.push_back(viewport);
        pipelineConfig.scissors.push_back(scissorRect);
        pipelineConfig.depthStencilState = pipelineDepthStencilStateCreateInfo;
        pipelineConfig.colorAttachmentBlendStates.push_back(pipelineColorBlendState);
        pipelineConfig.colorBlendState = pipelineColorBlendStateCreateInfo;
        pipelineConfig.inputAssemblyState = pipelineInputAssemblyStateCreateInfo;
        pipelineConfig.rasterizationState = pipelineRasterizationStateCreateInfo;
        pipelineConfig.multisampleState = pipelineMulisampleStateCreateInfo;
    }

    Pipeline::Pipeline(Device& device)
        : m_kDevice { device }
    {
        assert(m_kSwapchain != nullptr && "Attempting to create Pipeline before proper initialization");
    }

    Pipeline::~Pipeline()
    {
        for (size_t i = 0; i < m_vkDescriptorSetLayouts.size(); ++i)
            vkDestroyDescriptorSetLayout(m_kDevice.getDevice(), m_vkDescriptorSetLayouts[i], nullptr);
        vkDestroyPipelineLayout(m_kDevice.getDevice(), m_vkPipelineLayout, nullptr);
        vkDestroyPipeline(m_kDevice.getDevice(), m_vkPipeline, nullptr);
    }

    void Pipeline::setupPipeline(const nlohmann::json& json, PipelineConfig& pipelineConfig)
    {
        // Create shader stages
        // std::vector<VkPipelineShaderStageCreateInfo> shaderStages;

        for (const auto& shaderStageInfo : json["shaders"])
        {
            if (shaderStageInfo["name"].empty())
                continue;
            
            VkPipelineShaderStageCreateInfo createInfo {};
            createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
            createInfo.stage = shaderStageInfo["stage"].get<VkShaderStageFlagBits>();
            createInfo.module = createShaderModule(m_kDevice.getDevice(), shaderStageInfo["name"].get<std::string>());
            // TODO: need to get entry point to work, char* gets dealllocated before pipeline is created, so right now we just hardcode
            // std::string str = shaderStageInfo["entryPoint"]; // .get<std::string>(); 
            createInfo.pName = "main"; // str.c_str();

            shaderStages.push_back(std::move(createInfo));
            m_vkShaderModules.push_back(createInfo.module);
        }

        // Create pipeline layout
        for (const auto& layoutInfo : json["descSetLayouts"])
        {
            std::vector<VkDescriptorSetLayoutBinding> descSetLayoutBindings;

            for (const auto& bindingInfo : layoutInfo)
            {
                VkDescriptorSetLayoutBinding layoutBinding {};
                layoutBinding.binding = bindingInfo["binding"].get<uint32_t>();
                layoutBinding.descriptorType = bindingInfo["descriptorType"].get<VkDescriptorType>();
                layoutBinding.descriptorCount = bindingInfo["descriptorCount"].get<uint32_t>();
                layoutBinding.stageFlags = bindingInfo["stageFlags"].get<VkShaderStageFlags>();

                descSetLayoutBindings.push_back(std::move(layoutBinding));
            }

            VkDescriptorSetLayoutCreateInfo createInfo {};
            createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
            createInfo.bindingCount = static_cast<uint32_t>(descSetLayoutBindings.size()); 
            createInfo.pBindings = descSetLayoutBindings.data();

            VkDescriptorSetLayout layout;
            VK_CHECK(vkCreateDescriptorSetLayout(m_kDevice.getDevice(), &createInfo, nullptr, &layout));

            m_vkDescriptorSetLayouts.push_back(layout);
        }

        VkPipelineLayoutCreateInfo layoutCreateInfo {};
        layoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        layoutCreateInfo.setLayoutCount = static_cast<uint32_t>(m_vkDescriptorSetLayouts.size());
        layoutCreateInfo.pSetLayouts = m_vkDescriptorSetLayouts.data();
        layoutCreateInfo.pushConstantRangeCount = 0u;
        layoutCreateInfo.pPushConstantRanges = nullptr;

        VK_CHECK(vkCreatePipelineLayout(m_kDevice.getDevice(), &layoutCreateInfo, nullptr, &m_vkPipelineLayout));

        pipelineConfig.stageCount = static_cast<uint32_t>(shaderStages.size());
        pipelineConfig.shaderStages = std::move(shaderStages);
        pipelineConfig.layout = m_vkPipelineLayout;

        defaultPipelineInit(pipelineConfig, m_kSwapchain->getExtent());
    }

    void Pipeline::createPipeline(PipelineConfig& pipelineConfig)
    {
        const VkPipelineViewportStateCreateInfo pipelineViewportStateCreateInfo {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
            .viewportCount = static_cast<uint32_t>(pipelineConfig.viewports.size()),
            .pViewports = pipelineConfig.viewports.data(),
            .scissorCount = static_cast<uint32_t>(pipelineConfig.viewports.size()),
            .pScissors = pipelineConfig.scissors.data(),
        };

        const VkPipelineVertexInputStateCreateInfo pipelineVertexInputStateCreateInfo {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
            .vertexBindingDescriptionCount = static_cast<uint32_t>(pipelineConfig.vertexInputBindings.size()),
            .pVertexBindingDescriptions = pipelineConfig.vertexInputBindings.data(),
            .vertexAttributeDescriptionCount = static_cast<uint32_t>(pipelineConfig.vertexInputAttribtutes.size()),
            .pVertexAttributeDescriptions = pipelineConfig.vertexInputAttribtutes.data()
        };

        pipelineConfig.colorBlendState.attachmentCount = static_cast<uint32_t>(pipelineConfig.colorAttachmentBlendStates.size());
        pipelineConfig.colorBlendState.pAttachments = pipelineConfig.colorAttachmentBlendStates.data();

        VkGraphicsPipelineCreateInfo createInfo {};
        createInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        createInfo.stageCount = pipelineConfig.stageCount;
        createInfo.pStages = pipelineConfig.shaderStages.data();
        createInfo.pVertexInputState = &pipelineVertexInputStateCreateInfo;
        createInfo.pInputAssemblyState = &pipelineConfig.inputAssemblyState;
        createInfo.pTessellationState = nullptr;
        createInfo.pViewportState = &pipelineViewportStateCreateInfo;
        createInfo.pRasterizationState = &pipelineConfig.rasterizationState;
        createInfo.pMultisampleState = &pipelineConfig.multisampleState;
        createInfo.pDepthStencilState = &pipelineConfig.depthStencilState;
        createInfo.pColorBlendState = &pipelineConfig.colorBlendState;
        createInfo.pDynamicState = nullptr;
        createInfo.layout = pipelineConfig.layout;
        createInfo.renderPass = pipelineConfig.renderPass;
        createInfo.subpass = 0u;
        createInfo.basePipelineHandle = VK_NULL_HANDLE;
        createInfo.basePipelineIndex = 0u;

        VK_CHECK(vkCreateGraphicsPipelines(m_kDevice.getDevice(), VK_NULL_HANDLE, 1u, &createInfo, nullptr, &m_vkPipeline));

        for (size_t i = 0; i < m_vkShaderModules.size(); ++i)
            vkDestroyShaderModule(m_kDevice.getDevice(), m_vkShaderModules[i], nullptr);
    }
};