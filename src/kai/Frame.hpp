#ifndef KAI_FRAME_HPP
#define KAI_FRAME_HPP

#include <vulkan/vulkan.h>


namespace kai
{
    class Device;
    class Swapchain;

    class Frame
    {
    private:
        static Device* m_kDevice;
        static Swapchain* m_kSwapchain;

        VkCommandPool m_vkCommandPool;
        VkCommandBuffer m_vkCommandBuffer;

        VkFence m_vkRendereCompleteFence;
        VkSemaphore m_vkImageAcquiredSem4;
        VkSemaphore m_vkRenderCompleteSem4;

    public:
        static void setDevice(Device* device)
        {
            m_kDevice = device;
        }

        static void setSwapchain(Swapchain* swapchain)
        {
            m_kSwapchain = swapchain;
        }

        Frame();
        ~Frame();

        void preRender();
        void postRender();

        VkCommandBuffer getCommandBuffer() const { return m_vkCommandBuffer; }
    };
};

#endif // KAI_FRAME_HPP