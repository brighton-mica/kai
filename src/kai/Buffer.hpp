#ifndef KAI_BUFFER_HPP
#define KAI_BUFFER_HPP

#include <vulkan/vulkan.h>

namespace kai
{
    class Buffer
    {
    private:
        static VkDevice* device;
        static VkPhysicalDeviceMemoryProperties* physDevMemProps;

        VkBuffer m_vkBuffer;
        VkDeviceMemory m_vkBufferMemory;
        VkMemoryPropertyFlags m_vkBufferMemProps;

        bool m_bDestroyed;
        bool m_bCreated;

    public:
        static void init(VkDevice* device, VkPhysicalDeviceMemoryProperties* physDevMemProps);

        Buffer();
        ~Buffer();
        Buffer(Buffer&& rhs);

        void create(const VkBufferCreateInfo& createInfo, const VkMemoryPropertyFlags memProperties);
        void destroy();
        void uploadData(const VkDeviceSize nbytes, const void* data);

        VkBuffer getBuffer() const { return m_vkBuffer; }
    };
} // namespace kai

#endif // KAI_BUFFER_HPP
