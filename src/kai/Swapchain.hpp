#ifndef KAI_SWAPCHAIN_HPP
#define KAI_SWAPCHAIN_HPP

#include <vulkan/vulkan.h>
#include <vector>
#include <assert.h>

namespace kai
{
    class Device;

    class Swapchain
    {
    private:
        Device& m_kDevice;
        VkSwapchainKHR m_vkSwapchain;
        VkSwapchainCreateInfoKHR m_vkSwapchainCreateInfo;

        uint32_t m_uNumImages;
        std::vector<VkImage> m_vkImages;
        std::vector<VkImageView> m_vkImageViews;

        uint32_t m_uImageIdx;

    public:
        Swapchain(Device& device, const uint32_t width, const uint32_t height, const uint32_t numBuffers);
        ~Swapchain();

        void acquireNextImage(VkSemaphore imageAcquiredSem4);
        void present(VkSemaphore signalSemaphore);

        uint32_t getNumImages() const { return m_uNumImages; }
        VkExtent2D getExtent() const { return m_vkSwapchainCreateInfo.imageExtent; }
        VkFormat getImageFormat() const { return m_vkSwapchainCreateInfo.imageFormat; }
        uint32_t getImageIdx() const { return m_uImageIdx; }
        VkImageView getImageView(const uint32_t idx) { assert(idx < m_vkImageViews.size()); return m_vkImageViews[idx]; }
    };
}


#endif // KAI_SWAPCHAIN_HPP