#include "Swapchain.hpp"
#include "Device.hpp"
#include "Frame.hpp"
#include "Pipeline.hpp"
#include "Defines.hpp"

#include <iostream>

namespace kai
{
    Swapchain::Swapchain(Device& device, const uint32_t width, const uint32_t height, const uint32_t numBuffers)
        : m_kDevice { device }
    {
        // Currently filled out with desired traits
        m_vkSwapchainCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
            .surface = m_kDevice.getSurface(),
            .minImageCount = 2,
            .imageFormat = VK_FORMAT_B8G8R8_UNORM,
            .imageColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR,
            .imageExtent = {
                .width = static_cast<uint32_t>(width),
                .height = static_cast<uint32_t>(height),
            },
            .imageArrayLayers = 1u,
            .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
            .imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
            .queueFamilyIndexCount = 0,
            .pQueueFamilyIndices = nullptr,
            .preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR,
            .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
            .presentMode = VK_PRESENT_MODE_FIFO_KHR,
            .clipped = VK_TRUE,
            .oldSwapchain = VK_FALSE,
        };

        VkSurfaceCapabilitiesKHR surfaceCapabilities;
        VK_CHECK(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(m_kDevice.getPhysicalDevice(), m_kDevice.getSurface(), &surfaceCapabilities));

        //** Image Count
        {
            assert(m_vkSwapchainCreateInfo.minImageCount > 0 && "Invalid requested image count for swapchain!");

            // If the minImageCount is 0, then there is not a limit on the number of images the swapchain
            // can support (ignoring memory constraints). See the Vulkan Spec for more information.
            if (surfaceCapabilities.maxImageCount == 0)
            {
                if (m_vkSwapchainCreateInfo.minImageCount < surfaceCapabilities.minImageCount)
                {
                    std::cout << "Failed to create Swapchain. The requested number of images " << m_vkSwapchainCreateInfo.minImageCount << " does not meet the minimum requirement of " << surfaceCapabilities.minImageCount << '\n';
                    exit(EXIT_FAILURE);
                }
            }
            else if (m_vkSwapchainCreateInfo.minImageCount < surfaceCapabilities.minImageCount &&
                    m_vkSwapchainCreateInfo.minImageCount > surfaceCapabilities.maxImageCount)
            {
                std::cout << "The number of requested Swapchain images " << m_vkSwapchainCreateInfo.minImageCount << " is not supported. Min: " << surfaceCapabilities.minImageCount << " Max: " << surfaceCapabilities.maxImageCount << '\n';
                exit(EXIT_FAILURE);
            }
        }

        //** Image Format
        {
            uint32_t numSupportedSurfaceFormats = 0;
            VK_CHECK(vkGetPhysicalDeviceSurfaceFormatsKHR(m_kDevice.getPhysicalDevice(), m_kDevice.getSurface(), &numSupportedSurfaceFormats, nullptr));
            VkSurfaceFormatKHR* supportedSurfaceFormats = new VkSurfaceFormatKHR[numSupportedSurfaceFormats];
            VK_CHECK(vkGetPhysicalDeviceSurfaceFormatsKHR(m_kDevice.getPhysicalDevice(), m_kDevice.getSurface(), &numSupportedSurfaceFormats, supportedSurfaceFormats));

            bool requestedFormatFound = false;
            for (uint32_t i = 0; i < numSupportedSurfaceFormats; ++i)
            {
                if (supportedSurfaceFormats[i].format == m_vkSwapchainCreateInfo.imageFormat)
                {
                    m_vkSwapchainCreateInfo.imageColorSpace = supportedSurfaceFormats[i].colorSpace;
                    requestedFormatFound = true;
                    break;
                }
            }

            if (!requestedFormatFound)
            {
                m_vkSwapchainCreateInfo.imageFormat = supportedSurfaceFormats[0].format;
                m_vkSwapchainCreateInfo.imageColorSpace = supportedSurfaceFormats[0].colorSpace;
            }

            delete [] supportedSurfaceFormats;
        }

        //** Extent
        {
            // The Vulkan Spec states that if the current width/height is 0xFFFFFFFF, then the surface size
            // will be deteremined by the extent specified in the Vkm_vkSwapchainCreateInfoKHR.
            if (surfaceCapabilities.currentExtent.width == (uint32_t)-1)
            {
                m_vkSwapchainCreateInfo.imageExtent = surfaceCapabilities.currentExtent;
            }
        }

        //** Pre Transform
        {
            if (surfaceCapabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
            {
                m_vkSwapchainCreateInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
            }
            else
            {
                m_vkSwapchainCreateInfo.preTransform = surfaceCapabilities.currentTransform;
                printf("WARNING - Swapchain pretransform is not IDENTITIY_BIT_KHR!\n");
            }
        }

        //** Composite Alpha
        {
            // Determine the composite alpha format the application needs.
            // Find a supported composite alpha format (not all devices support alpha opaque),
            // but we prefer it.
            // Simply select the first composite alpha format available
            // Used for blending with other windows in the system
            VkCompositeAlphaFlagBitsKHR compositeAlphaFlags[4] = {
                VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
                VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR,
                VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR,
                VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR,
            };
            for (size_t i = 0; i < 4; ++i)
            {
                if (surfaceCapabilities.supportedCompositeAlpha & compositeAlphaFlags[i]) 
                {
                    m_vkSwapchainCreateInfo.compositeAlpha = compositeAlphaFlags[i];
                    break;
                };
            }
        }

        //** Present Mode
        {
            uint32_t numSupportedPresentModes = 0;
            VK_CHECK(vkGetPhysicalDeviceSurfacePresentModesKHR(m_kDevice.getPhysicalDevice(), m_kDevice.getSurface(), &numSupportedPresentModes, nullptr));
            VkPresentModeKHR* supportedPresentModes = new VkPresentModeKHR[numSupportedPresentModes];
            VK_CHECK(vkGetPhysicalDeviceSurfacePresentModesKHR(m_kDevice.getPhysicalDevice(), m_kDevice.getSurface(), &numSupportedPresentModes, supportedPresentModes));

            // Determine the present mode the application needs.
            // Try to use mailbox, it is the lowest latency non-tearing present mode
            // All devices support FIFO (this mode waits for the vertical blank or v-sync)
            m_vkSwapchainCreateInfo.presentMode = VK_PRESENT_MODE_FIFO_KHR;
            for (uint32_t i = 0; i < numSupportedPresentModes; ++i)
            {
                if (supportedPresentModes[i] == m_vkSwapchainCreateInfo.presentMode)
                {
                    m_vkSwapchainCreateInfo.presentMode = m_vkSwapchainCreateInfo.presentMode;
                    break;
                }
            }

            delete [] supportedPresentModes;
        }

        VK_CHECK(vkCreateSwapchainKHR(m_kDevice.getDevice(), &m_vkSwapchainCreateInfo, nullptr, &m_vkSwapchain));

        VK_CHECK(vkGetSwapchainImagesKHR(m_kDevice.getDevice(), m_vkSwapchain, &m_uNumImages, nullptr));
        m_vkImages.resize(m_uNumImages);
        m_vkImageViews.resize(m_uNumImages);
        VK_CHECK(vkGetSwapchainImagesKHR(m_kDevice.getDevice(), m_vkSwapchain, &m_uNumImages, m_vkImages.data()));

        VkImageViewCreateInfo imageViewCreateInfo {
            .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
            .viewType = VK_IMAGE_VIEW_TYPE_2D,
            .format = m_vkSwapchainCreateInfo.imageFormat,
            .components = {
                .r = VK_COMPONENT_SWIZZLE_IDENTITY,
                .g = VK_COMPONENT_SWIZZLE_IDENTITY,
                .b = VK_COMPONENT_SWIZZLE_IDENTITY,
                .a = VK_COMPONENT_SWIZZLE_IDENTITY },
            .subresourceRange = {
                .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                .baseMipLevel = 0,
                .levelCount = 1,
                .baseArrayLayer = 0,
                .layerCount = 1 },
        };

        for (size_t i = 0; i < m_uNumImages; ++i)
        {
            imageViewCreateInfo.image = m_vkImages[i];
            VK_CHECK(vkCreateImageView(m_kDevice.getDevice(), &imageViewCreateInfo, nullptr, &m_vkImageViews[i]));
        }

        std::cout << "Swapchain Image Count: " << m_uNumImages << '\n';

        Frame::setSwapchain(this); 
        Pipeline::setSwapchain(this);
    }

    Swapchain::~Swapchain()
    {
        for (uint32_t i = 0; i < m_uNumImages; ++i)
            vkDestroyImageView(m_kDevice.getDevice(), m_vkImageViews[i], nullptr);

        vkDestroySwapchainKHR(m_kDevice.getDevice(), m_vkSwapchain, nullptr);
    }

    void Swapchain::acquireNextImage(VkSemaphore imageAcquiredSem4)
    {
        VK_CHECK(vkAcquireNextImageKHR(
            m_kDevice.getDevice(),
            m_vkSwapchain,
            UINT64_MAX,
            imageAcquiredSem4,
            VK_NULL_HANDLE,
            &m_uImageIdx));
    }

    void Swapchain::present(VkSemaphore signalSemaphore)
    {
        const VkPresentInfoKHR presentInfo {
            .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
            .waitSemaphoreCount = 1u,
            .pWaitSemaphores = &signalSemaphore,
            .swapchainCount = 1u,
            .pSwapchains = &m_vkSwapchain,
            .pImageIndices = &m_uImageIdx,
            .pResults = nullptr,
        };

        VK_CHECK(vkQueuePresentKHR(m_kDevice.getQueue(VK_QUEUE_GRAPHICS_BIT), &presentInfo));
    }
};