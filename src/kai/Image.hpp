#ifndef KAI_IMAGE_HPP
#define KAI_IMAGE_HPP

#include <vulkan/vulkan.h>

namespace kai
{
    class Image
    {
    private:
        static VkDevice* device;
        static VkPhysicalDeviceMemoryProperties* physDevMemProps;

        VkImage m_vkImage;
        VkImageView m_vkImageView;
        VkDeviceMemory m_vkImageMemory;

    public:
        static void init(VkDevice* device, VkPhysicalDeviceMemoryProperties* physDevMemProps);
        
        Image();

        void create(const VkImageCreateInfo& imageCreateInfo, VkImageViewCreateInfo& imageViewCreateInfo);
        void destroy();

        const VkImageView& getImageView() const { return m_vkImageView; }
    };
};

#endif // KAI_IMAGE_HPP