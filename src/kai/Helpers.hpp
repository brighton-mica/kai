#ifndef HELPERS_HPP
#define HELPERS_HPP

#include <vulkan/vulkan.hpp>

VkShaderModule createShaderModule(VkDevice device, const char* filename)
{
    FILE* f = fopen(filename, "r");
    if (f == NULL)
    {
        printf("Failed to open file %s!\n", filename);
        exit(EXIT_FAILURE);
    }

    fseek(f, 0, SEEK_END);
    const size_t nbytes_file_size = (size_t)ftell(f);
    rewind(f);

    uint32_t* buffer = (uint32_t*)malloc(nbytes_file_size);
    fread(buffer, nbytes_file_size, 1, f);
    fclose(f);

    VkShaderModuleCreateInfo ci_shader_module;
    ci_shader_module.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    ci_shader_module.pNext = NULL;
    ci_shader_module.flags = 0x0;
    ci_shader_module.codeSize = nbytes_file_size;
    ci_shader_module.pCode = buffer;

    VkShaderModule vk_shader_module;
    VkResult result = vkCreateShaderModule(device, &ci_shader_module, NULL, &vk_shader_module);
    if (result != VK_SUCCESS)
    {
        printf("Failed to create shader module for %s!\n", filename);
        exit(EXIT_FAILURE);
    }

    free(buffer);

    return vk_shader_module;
}

#endif // HELPERS_HPP