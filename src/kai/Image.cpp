#include "Image.hpp"

#include <assert.h>

#define VK_CHECK( val ) do {         \
    if ( val != VK_SUCCESS )         \
    {                                \
        assert( val == VK_SUCCESS ); \
    }                                \
} while( false )                     \


uint32_t getHeapIndex(const uint32_t memoryTypeIndices, const VkMemoryPropertyFlags memoryPropertyFlags, VkPhysicalDeviceMemoryProperties memoryProperties)
{
	// Iterate over all memory types available for the device used in this example
	for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; i++)
	{
		if (memoryTypeIndices & (1 << i) && (memoryProperties.memoryTypes[i].propertyFlags & memoryPropertyFlags) == memoryPropertyFlags)
		{
			return i;
		}
	}

    assert( false && "Could not find suitable memory type!");
    return 0;
}


namespace kai
{
    VkDevice* Image::device = nullptr;
    VkPhysicalDeviceMemoryProperties* Image::physDevMemProps = nullptr;

    void Image::init(VkDevice* _device, VkPhysicalDeviceMemoryProperties* _physDevMemProps) 
    { 
        device = _device;
        physDevMemProps = _physDevMemProps;
    }

    Image::Image()
        : m_vkImage {VK_NULL_HANDLE}
        , m_vkImageView {VK_NULL_HANDLE}
        , m_vkImageMemory {VK_NULL_HANDLE}
    {

    }

    void Image::create(const VkImageCreateInfo& imageCreateInfo, VkImageViewCreateInfo& imageViewCreateInfo)
    {
        assert((device != nullptr && physDevMemProps != nullptr) 
                && "Attempting to create image before proper init");

        VK_CHECK(vkCreateImage(*device, &imageCreateInfo, nullptr, &m_vkImage));

        VkMemoryRequirements memReqs;
        vkGetImageMemoryRequirements(*device, m_vkImage, &memReqs);

        const VkMemoryAllocateInfo allocateInfo = { 
            .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
            .allocationSize = memReqs.size,
            .memoryTypeIndex = getHeapIndex(memReqs.memoryTypeBits, 
                                            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                                            *physDevMemProps)
        };

        VK_CHECK(vkAllocateMemory(*device, &allocateInfo, nullptr, &m_vkImageMemory));
        VK_CHECK(vkBindImageMemory(*device, m_vkImage, m_vkImageMemory, 0));

        imageViewCreateInfo.image = m_vkImage;

        VK_CHECK(vkCreateImageView(*device, &imageViewCreateInfo, nullptr,
                                    &m_vkImageView));
    }


    void Image::destroy()
    {
        vkDestroyImageView(*device, m_vkImageView, nullptr);
        vkFreeMemory(*device, m_vkImageMemory, nullptr);
        vkDestroyImage(*device, m_vkImage, nullptr);
    }
}