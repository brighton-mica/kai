#include "Buffer.hpp"

#include <assert.h>
#include <string.h>
#include <iostream>

#define VK_CHECK( val ) do {         \
    if ( val != VK_SUCCESS )         \
    {                                \
        assert( val == VK_SUCCESS ); \
    }                                \
} while( false )                     \


static uint32_t getHeapIndex(const uint32_t memoryTypeIndices, const VkMemoryPropertyFlags memoryPropertyFlags, VkPhysicalDeviceMemoryProperties memoryProperties)
{
	// Iterate over all memory types available for the device used in this example
	for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; i++)
	{
		if (memoryTypeIndices & (1 << i) && (memoryProperties.memoryTypes[i].propertyFlags & memoryPropertyFlags) == memoryPropertyFlags)
		{
			return i;
		}
	}

    assert( false && "Could not find suitable memory type!");
    return 0;
}

namespace kai
{
    VkDevice* Buffer::device = nullptr;
    VkPhysicalDeviceMemoryProperties* Buffer::physDevMemProps = nullptr;

    void Buffer::init(VkDevice* _device, VkPhysicalDeviceMemoryProperties* _physDevMemProps)
    {
        device = _device;
        physDevMemProps = _physDevMemProps;
    }

    Buffer::Buffer()
        : m_vkBuffer { VK_NULL_HANDLE }
        , m_vkBufferMemory { VK_NULL_HANDLE }
        , m_vkBufferMemProps { VK_NULL_HANDLE }
        , m_bCreated { false }
        , m_bDestroyed { false }
    {
    }

    Buffer::~Buffer()
    {
        if (!m_bDestroyed && m_bCreated)
        {
            vkDestroyBuffer(*device, m_vkBuffer, nullptr);
            vkFreeMemory(*device, m_vkBufferMemory, nullptr);
        }
    }

    Buffer::Buffer(Buffer&& rhs)
        : m_vkBuffer { std::move(rhs.m_vkBuffer) }
        , m_vkBufferMemory { std::move(rhs.m_vkBufferMemory) }
        , m_vkBufferMemProps { std::move(rhs.m_vkBufferMemProps) }
        , m_bCreated { std::move(rhs.m_bCreated) }
        , m_bDestroyed { std::move(rhs.m_bDestroyed) }
    {
        rhs.m_vkBuffer = VK_NULL_HANDLE;
        rhs.m_vkBufferMemory = VK_NULL_HANDLE;
        rhs.m_bCreated = false;
        rhs.m_bDestroyed = false;
    }

    void Buffer::create(const VkBufferCreateInfo& createInfo, const VkMemoryPropertyFlags memProperties)
    {
        assert((device != nullptr && physDevMemProps != nullptr)
                && "Attempting to create buffer before proper init");

        m_vkBufferMemProps = memProperties;

        VK_CHECK(vkCreateBuffer(*device, &createInfo, nullptr, &m_vkBuffer));

        VkMemoryRequirements memReqs;
        vkGetBufferMemoryRequirements(*device, m_vkBuffer, &memReqs);

        const VkMemoryAllocateInfo allocInfo {
            .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
            .allocationSize = memReqs.size,
            .memoryTypeIndex = getHeapIndex(memReqs.memoryTypeBits, m_vkBufferMemProps, *physDevMemProps),
        }; 

        VK_CHECK(vkAllocateMemory(*device, &allocInfo, nullptr, &m_vkBufferMemory));
        VK_CHECK(vkBindBufferMemory(*device, m_vkBuffer, m_vkBufferMemory, 0));

        m_bCreated = true;
    }

    void Buffer::destroy()
    {
        vkDestroyBuffer(*device, m_vkBuffer, nullptr);
        vkFreeMemory(*device, m_vkBufferMemory, nullptr);
        m_bDestroyed = true;
        m_bCreated = false;
    }

    void Buffer::uploadData(const VkDeviceSize nbytes, const void* data)
    {
        assert((m_vkBuffer != VK_NULL_HANDLE && m_vkBufferMemory != VK_NULL_HANDLE)
            && "Attempting to upload data to buffer before buffer creation!");

        if (m_vkBufferMemProps & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
        {
            // Require Staging Buffer Upload
            std::cerr << "Device Local Buffers are not supported yet!\n";
            return;
        }
        else if (m_vkBufferMemProps & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
        {
            // Mapped Memory
            void* bufferData;
            vkMapMemory(*device, m_vkBufferMemory, 0, VK_WHOLE_SIZE, 0, &bufferData);
            memcpy(bufferData, data, nbytes);

            VkMappedMemoryRange range {
                .sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
                .memory = m_vkBufferMemory,
                .offset = 0,
                .size = VK_WHOLE_SIZE,
            };

            vkFlushMappedMemoryRanges(*device, 1, &range);
            vkUnmapMemory(*device, m_vkBufferMemory);
        }
    }
} // namespace kai


