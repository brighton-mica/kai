#ifndef KAI_GUI_HPP
#define KAI_GUI_HPP

#include <vulkan/vulkan.h>
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_vulkan.h"

namespace kai
{
    class Device;

    class Gui
    {
    private:
        Device& m_kDevice;

        VkRenderPass m_vkRenderPass;
        VkDescriptorPool m_vkDescPool;
    public:
        Gui(GLFWwindow* window, Device& device);
        ~Gui();

        void init(const uint32_t numSwapchainImages, VkRenderPass renderPasss);
        void record(VkCommandBuffer commandBuffer);
    };
}


#endif // KAI_GUI_HPP`