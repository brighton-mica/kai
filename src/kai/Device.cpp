#include "Device.hpp"
#include "Defines.hpp"
#include "Buffer.hpp"
#include "Image.hpp"
#include "Frame.hpp"

#include <GLFW/glfw3.h>
#include <assert.h>
#include <iostream>

namespace kai
{
    Device::Device(GLFWwindow* window)
    {
        // Instance
        {
            uint32_t numInstanceExtensions = 0;
            const char** instanceExtensions = glfwGetRequiredInstanceExtensions(&numInstanceExtensions);

            const uint32_t numInstanceLayers = 1;
            const char* instanceLayers[1] = { "VK_LAYER_KHRONOS_validation" };

            const VkApplicationInfo appInfo {
                .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
                .pApplicationName = "Triangle",
                .applicationVersion = VK_MAKE_VERSION(1, 0, 0),
                .pEngineName = "Kai",
                .engineVersion = VK_MAKE_VERSION(1, 0, 0),
                .apiVersion = VK_API_VERSION_1_2,
            };

            const VkInstanceCreateInfo createInfo {
                .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
                .pApplicationInfo = &appInfo,
                .enabledLayerCount = numInstanceLayers,
                .ppEnabledLayerNames = instanceLayers,
                .enabledExtensionCount = numInstanceExtensions,
                .ppEnabledExtensionNames = instanceExtensions,
            };

            VK_CHECK(vkCreateInstance(&createInfo, nullptr, &m_vkInstance));

            VK_CHECK(glfwCreateWindowSurface(m_vkInstance, window, nullptr, &m_vkSurface));
        }

        // Physical Device
        {
            uint32_t numPhysicalDevices = 0;
            vkEnumeratePhysicalDevices(m_vkInstance, &numPhysicalDevices, nullptr);
            VkPhysicalDevice* physicalDevices = new VkPhysicalDevice[numPhysicalDevices];
            vkEnumeratePhysicalDevices(m_vkInstance, &numPhysicalDevices, physicalDevices);

            // for (uint32_t i = 0; i < numPhysicalDevices; ++i)
            // {
            //     VkPhysicalDeviceProperties physicalDeviceProps;
            //     vkGetPhysicalDeviceProperties(physicalDevices[i], &physicalDeviceProps);
            //     std::cout << physicalDeviceProps.deviceName << '\n';
            // }

            m_vkPhysicalDevice = physicalDevices[2];
            delete [] physicalDevices;

            VkPhysicalDeviceProperties physicalDeviceProps;
            vkGetPhysicalDeviceProperties(m_vkPhysicalDevice, &physicalDeviceProps);

            std::cout << "Physical Device: " << physicalDeviceProps.deviceName << '\n';

            vkGetPhysicalDeviceMemoryProperties(m_vkPhysicalDevice, &m_vkPhysicalDeviceMemoryProps);

            uint32_t numQueueFamilyProperties = 0;
            vkGetPhysicalDeviceQueueFamilyProperties(m_vkPhysicalDevice, &numQueueFamilyProperties, nullptr);
            VkQueueFamilyProperties* queueFamilyProperties = new VkQueueFamilyProperties[numQueueFamilyProperties];
            vkGetPhysicalDeviceQueueFamilyProperties(m_vkPhysicalDevice, &numQueueFamilyProperties, queueFamilyProperties);

            uint32_t graphicsQueueFamilyIndex = UINT32_MAX;
            uint32_t presentQueueFamilyIndex = UINT32_MAX;
            for (uint32_t i = 0; i < numQueueFamilyProperties; ++i)
            {
                VkBool32 q_fam_supports_present = false;
                vkGetPhysicalDeviceSurfaceSupportKHR(m_vkPhysicalDevice, i, m_vkSurface, &q_fam_supports_present);

                if (queueFamilyProperties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT && q_fam_supports_present == VK_TRUE)
                {
                    graphicsQueueFamilyIndex = i;
                    presentQueueFamilyIndex  = i;
                    break;
                }
            }

            delete [] queueFamilyProperties;

            assert(graphicsQueueFamilyIndex < UINT32_MAX && "no supported graphics queue family index");
            assert(presentQueueFamilyIndex  < UINT32_MAX && "no supported present queue family index");
            assert(graphicsQueueFamilyIndex == presentQueueFamilyIndex && "queue families (graphics/present) do not match");

            m_grapicsQueue.familyIndex = graphicsQueueFamilyIndex;
        }

        // Device
        {
            const char* deviceExtensions[1] = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };
            const float queuePriority = 1.0f;

            const VkDeviceQueueCreateInfo queue = {
                .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
                .queueFamilyIndex = m_grapicsQueue.familyIndex,
                .queueCount = 1,
                .pQueuePriorities = &queuePriority,
            };

            const VkDeviceCreateInfo deviceCreateInfo = {
                    .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
                    .queueCreateInfoCount = 1,
                    .pQueueCreateInfos = &queue,
                    .enabledLayerCount = 0,
                    .ppEnabledLayerNames = nullptr,
                    .enabledExtensionCount = 1,
                    .ppEnabledExtensionNames = deviceExtensions,
                    .pEnabledFeatures = nullptr
            };

            VK_CHECK(vkCreateDevice(m_vkPhysicalDevice, &deviceCreateInfo, nullptr, &m_vkDevice));

            vkGetDeviceQueue(m_vkDevice, m_grapicsQueue.familyIndex, 0, &m_grapicsQueue.vkQueue);
        }
    
        Image::init(&m_vkDevice, &m_vkPhysicalDeviceMemoryProps);
        Buffer::init(&m_vkDevice, &m_vkPhysicalDeviceMemoryProps);
        Frame::setDevice(this); 
    }

    Device::~Device()
    {
        vkDestroyDevice(m_vkDevice, nullptr);
        vkDestroySurfaceKHR(m_vkInstance, m_vkSurface, nullptr);
        vkDestroyInstance(m_vkInstance, nullptr);
    }

    VkQueue Device::getQueue(VkQueueFlags flag) const
    {
        switch (flag)
        {
            case VK_QUEUE_GRAPHICS_BIT:
                return m_grapicsQueue.vkQueue;
                break;
            default:
                assert(false && "Requested queue type does exist.");
                break;
        }
    }

    uint32_t Device::getQueueFamilyIdx(VkQueueFlags flag) const
    {
        switch (flag)
        {
            case VK_QUEUE_GRAPHICS_BIT:
                return m_grapicsQueue.familyIndex;
                break;
            default:
                assert(false && "Requested queue type does exist.");
                break;
        }
    }
}