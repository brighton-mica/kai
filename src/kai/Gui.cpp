#include "Gui.hpp"
#include "Device.hpp"

#define VK_CHECK( val ) do {         \
    if ( val != VK_SUCCESS )         \
    {                                \
        assert( val == VK_SUCCESS ); \
    }                                \
} while( false )                     \


class GLFWwindow;

namespace kai
{
    Gui::Gui(GLFWwindow* window, Device& device) 
        : m_kDevice { device }
    {
        // Create Dedicated Descriptor Pool
        {
            VkDescriptorPoolSize pool_sizes[] =
            {
                { VK_DESCRIPTOR_TYPE_SAMPLER, 1000 },
                { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1000 },
                { VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1000 },
                { VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1000 },
                { VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1000 },
                { VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1000 },
                { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1000 },
                { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1000 },
                { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1000 },
                { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1000 },
                { VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1000 }
            };
            VkDescriptorPoolCreateInfo pool_info = {};
            pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
            pool_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
            pool_info.maxSets = 1000 * IM_ARRAYSIZE(pool_sizes);
            pool_info.poolSizeCount = (uint32_t)IM_ARRAYSIZE(pool_sizes);
            pool_info.pPoolSizes = pool_sizes;

            VK_CHECK(vkCreateDescriptorPool(m_kDevice.getDevice(), &pool_info, nullptr, & m_vkDescPool));
        }

        IMGUI_CHECKVERSION();
        ImGui::CreateContext();
        ImGuiIO& io = ImGui::GetIO(); (void)io;
        ImGui_ImplGlfw_InitForVulkan(window, true);
    }

    Gui::~Gui()
    {
        vkDestroyDescriptorPool(m_kDevice.getDevice(), m_vkDescPool, nullptr);

        ImGui_ImplVulkan_Shutdown();
        ImGui_ImplGlfw_Shutdown();
        ImGui::DestroyContext();
    }

    void Gui::init(const uint32_t numSwapchainImages, VkRenderPass renderPass)
    {
        ImGui_ImplVulkan_InitInfo init_info = {};
        init_info.Instance = m_kDevice.getInstance();
        init_info.PhysicalDevice = m_kDevice.getPhysicalDevice();
        init_info.Device = m_kDevice.getDevice();
        init_info.QueueFamily = m_kDevice.getQueueFamilyIdx(VK_QUEUE_GRAPHICS_BIT);
        init_info.Queue = m_kDevice.getQueue(VK_QUEUE_GRAPHICS_BIT);
        init_info.PipelineCache = VK_NULL_HANDLE;
        init_info.DescriptorPool = m_vkDescPool;
        init_info.Allocator = nullptr;
        init_info.MinImageCount = numSwapchainImages;
        init_info.ImageCount = numSwapchainImages;
        init_info.CheckVkResultFn = nullptr;
        ImGui_ImplVulkan_Init(&init_info, renderPass);

        // Upload Fonts
        {
            VkCommandPool commandPool;
            VkCommandBuffer commandBuffer;

            {
                VkCommandPoolCreateInfo createInfo = {};
                createInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
                createInfo.queueFamilyIndex = m_kDevice.getQueueFamilyIdx(VK_QUEUE_GRAPHICS_BIT);

                VK_CHECK(vkCreateCommandPool(
                    m_kDevice.getDevice(),
                    &createInfo,
                    nullptr,
                    &commandPool));
            }

            {
                VkCommandBufferAllocateInfo allocInfo = {};
                allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
                allocInfo.commandPool = commandPool;
                allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
                allocInfo.commandBufferCount = 1u;

                VK_CHECK(vkAllocateCommandBuffers(
                    m_kDevice.getDevice(),
                    &allocInfo,
                    &commandBuffer));
            }

            VK_CHECK(vkResetCommandPool(m_kDevice.getDevice(), commandPool, 0));

            VkCommandBufferBeginInfo beginInfo = {};
            beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            beginInfo.flags |= VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

            VK_CHECK(vkBeginCommandBuffer(commandBuffer, &beginInfo));

            ImGui_ImplVulkan_CreateFontsTexture(commandBuffer);

            VkSubmitInfo submitInfo = {};
            submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
            submitInfo.commandBufferCount = 1;
            submitInfo.pCommandBuffers = &commandBuffer;
            VK_CHECK(vkEndCommandBuffer(commandBuffer));
            VK_CHECK(vkQueueSubmit(m_kDevice.getQueue(VK_QUEUE_GRAPHICS_BIT), 1, &submitInfo, VK_NULL_HANDLE));
            VK_CHECK(vkDeviceWaitIdle(m_kDevice.getDevice()));

            ImGui_ImplVulkan_DestroyFontUploadObjects();

            vkDestroyCommandPool(m_kDevice.getDevice(), commandPool, nullptr);
        }
    }

    void Gui::record(VkCommandBuffer commandBuffer)
    {
        ImGui_ImplVulkan_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        bool showDemoWindow = true;
        ImGui::ShowDemoWindow(&showDemoWindow);
        
        ImGui::Render();
        ImDrawData* drawData = ImGui::GetDrawData();
        ImGui_ImplVulkan_RenderDrawData(drawData, commandBuffer);
    }
}