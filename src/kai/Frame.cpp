#include "Frame.hpp"
#include "Device.hpp"
#include "Swapchain.hpp"
#include "Defines.hpp"

#include <assert.h>
#include <iostream>

namespace kai
{
    Device* Frame::m_kDevice = nullptr;
    Swapchain* Frame::m_kSwapchain = nullptr;

    Frame::Frame()
    {
        assert(m_kDevice != nullptr && m_kSwapchain != nullptr
        && "Attempting to create frame before proprer initialization!");

        {
            VkCommandPoolCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
            createInfo.queueFamilyIndex = m_kDevice->getQueueFamilyIdx(VK_QUEUE_GRAPHICS_BIT);

            VK_CHECK(vkCreateCommandPool(
                m_kDevice->getDevice(),
                &createInfo,
                nullptr,
                &m_vkCommandPool));
        }

        {
            VkCommandBufferAllocateInfo allocInfo = {};
            allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
            allocInfo.commandPool = m_vkCommandPool;
            allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
            allocInfo.commandBufferCount = 1u;

            VK_CHECK(vkAllocateCommandBuffers(
                m_kDevice->getDevice(),
                &allocInfo,
                &m_vkCommandBuffer));
        }

        {
            VkSemaphoreCreateInfo createInfo = { VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO };
            VK_CHECK(vkCreateSemaphore(
                m_kDevice->getDevice(),
                &createInfo,
                nullptr,
                &m_vkImageAcquiredSem4));

            VK_CHECK(vkCreateSemaphore(
                m_kDevice->getDevice(),
                &createInfo,
                nullptr,
                &m_vkRenderCompleteSem4));
        }

        {
            VkFenceCreateInfo createInfo = { VK_STRUCTURE_TYPE_FENCE_CREATE_INFO };
            createInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
            VK_CHECK(vkCreateFence(
                m_kDevice->getDevice(),
                &createInfo,
                nullptr,
                &m_vkRendereCompleteFence));
        }
    }
    
    Frame::~Frame()
    {
        vkDestroyFence(m_kDevice->getDevice(), m_vkRendereCompleteFence, nullptr);
        vkDestroySemaphore(m_kDevice->getDevice(), m_vkRenderCompleteSem4, nullptr);
        vkDestroySemaphore(m_kDevice->getDevice(), m_vkImageAcquiredSem4, nullptr);
        vkDestroyCommandPool(m_kDevice->getDevice(), m_vkCommandPool, nullptr);
    }

    void Frame::preRender()
    {
        m_kSwapchain->acquireNextImage(m_vkImageAcquiredSem4);

        VK_CHECK(vkWaitForFences(m_kDevice->getDevice(), 1u, &m_vkRendereCompleteFence, VK_TRUE, UINT64_MAX));
        VK_CHECK(vkResetFences(m_kDevice->getDevice(), 1u, &m_vkRendereCompleteFence));

        VK_CHECK(vkResetCommandPool(m_kDevice->getDevice(), m_vkCommandPool, 0x0));
        
        static VkCommandBufferBeginInfo beginInfo = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
            .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT
        };

        VK_CHECK(vkBeginCommandBuffer(m_vkCommandBuffer, &beginInfo));
    }

    void Frame::postRender()
    {
        VK_CHECK(vkEndCommandBuffer(m_vkCommandBuffer));

        const VkPipelineStageFlags waitStages[1] { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
        const VkSubmitInfo submitInfo {
            .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
            .waitSemaphoreCount = 1u,
            .pWaitSemaphores = &m_vkImageAcquiredSem4,
            .pWaitDstStageMask = waitStages,
            .commandBufferCount = 1u,
            .pCommandBuffers = &m_vkCommandBuffer,
            .signalSemaphoreCount = 1u,
            .pSignalSemaphores = &m_vkRenderCompleteSem4,
        };

        VK_CHECK(vkQueueSubmit(m_kDevice->getQueue(VK_QUEUE_GRAPHICS_BIT), 1, &submitInfo, m_vkRendereCompleteFence));

        m_kSwapchain->present(m_vkRenderCompleteSem4);
    }
};