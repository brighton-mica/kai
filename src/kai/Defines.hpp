#ifndef KAI_DEFINES_HPP
#define KAI_DEFINES_HPP

#include <assert.h>

#define VK_CHECK( val ) do {         \
    if ( val != VK_SUCCESS )         \
    {                                \
        assert( val == VK_SUCCESS ); \
    }                                \
} while( false )                     \

#define EXIT( msg ) do { \
    std::cerr << msg << '\n'; \
    exit( EXIT_FAILURE ); \
} while ( false ) \

#endif // KAI_DEFINES_HPP