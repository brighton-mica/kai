#include "kai/Device.hpp"
#include "kai/Swapchain.hpp"
#include "kai/Frame.hpp"
// #include "kai/Camera.hpp"

#include "State.hpp"

#include <GLFW/glfw3.h>

#include <assert.h>
#include <chrono>
#include <iostream>
#include <array>

constexpr uint32_t NUM_BUFFERS = 2;

class App
{
private:
    kai::Device m_kDevice;
    kai::Swapchain m_kSwapchain;
    kai::Frame m_kFrames[NUM_BUFFERS];
    // kai::Camera m_kCamera;

    uint32_t m_uFrameIdx;
    State m_kState;

public:
    App(GLFWwindow* window, const uint32_t windowWidth, const uint32_t windowHeight);

    void render();
    void cleanup();
};