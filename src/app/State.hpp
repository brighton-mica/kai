#ifndef APP_STATE_HPP
#define APP_STATE_HPP

#include "kai/Device.hpp"
#include "kai/Swapchain.hpp"
#include "kai/Image.hpp"
#include "kai/Buffer.hpp"
#include "kai/Gui.hpp"

#include "render-systems/SimpleRenderSystem.hpp"

#include <vulkan/vulkan.h>
#include <vector>

struct BaseMaterial
{
    static VkDevice s_vkDevice;

    uint32_t id;
    std::string name;
    std::vector<std::string> shaders;

    std::vector<VkDescriptorSetLayout> m_vkDescriptorSetLayouts;
    VkPipelineLayout m_vkPipelineLayout;
    VkPipeline m_vkPipeline;

    uint32_t uboSize;

    // When MaterialInstances are created, the MaterialInstance ubo member variables need to stored in 
    // the associated MaterialInstances buffer. In order to know what members are stored where within the
    // buffer, we keep track of base-material ubo member info here (which all material-instances share).
    struct UboMemberInfo 
    {
        uint32_t offset;
        uint32_t size;
    };

    std::unordered_map<std::string, UboMemberInfo> uboMemberInfos;

    // A MaterialInstance is a unique configuration of the per-material set (set 1) in the base material's shaders
    // Right now it assumes the per-material set is a single ubo.
    // It is really just a container for the raw data that will be bound via descriptors.
    struct MaterialInstance
    {
        uint32_t id;
        std::string name;
        std::vector<unsigned char> uboData;
        kai::Buffer buffer;
        VkDescriptorSet descriptorSet;
    };

    // key = material-instance-id, value = material-instances associated with this base-material (specified in "material-instances.json")
    std::unordered_map<uint32_t, MaterialInstance> materialInstances;

    // We store descriptor sets in the base material because we can allocate all sets for a given basee material in one
    // allocation. Material-instances will refer to sets in this list.
    std::vector<VkDescriptorSet> m_vkDescriptorSets;

    ~BaseMaterial()
    {
        vkDestroyPipeline(s_vkDevice, m_vkPipeline, nullptr);
        vkDestroyPipelineLayout(s_vkDevice, m_vkPipelineLayout, nullptr);
        for (uint32_t i = 0; i < m_vkDescriptorSetLayouts.size(); ++i)
            vkDestroyDescriptorSetLayout(s_vkDevice, m_vkDescriptorSetLayouts[i], nullptr);
    }
};

struct Renderable
{
    uint32_t baseMaterialId;
    uint32_t materialInstanceId;
    std::string name;

    VkDescriptorSet m_vkDescriptorSet;
    kai::Buffer vertexBuffer;
};

class BaseMaterialRenderBin
{
    std::vector<Renderable> renderables;
};


class State
{
private:
    kai::Device& m_kDevice;
    kai::Swapchain& m_kSwapchain;
    kai::Image m_kDepthImage;
    kai::Gui gui;

    VkRenderPass m_vkRenderPass;
    VkRenderPassBeginInfo m_vkRenderPassBeginInfo;;
    std::vector<VkFramebuffer> m_vkFramebuffers;

    VkDescriptorPool m_vkMaterialDescriptorPool;

    kai::Buffer m_kFrameUbo;
    VkDescriptorSetLayout m_vkFrameDescriptorSetLayout;
    VkDescriptorSet m_vkFrameDescriptorSet;
    VkDescriptorPool m_vkFrameDescriptorPool;

    const uint32_t m_uNumBuffers;

    std::unordered_map<uint32_t, std::unordered_map<uint32_t, std::vector<Renderable>>> renderBins;
    std::unordered_map<uint32_t, BaseMaterial> baseMaterials;

    void createDepthAttachment();
    void createRenderPass();
    void createFramebuffers();
    void createFrameDescriptorSet();
    void createMaterials();
    void createScene();

public:
    State(GLFWwindow* window, kai::Device& device, kai::Swapchain& swapchain, const uint32_t numBuffers);
    ~State();

    void executeFrame(const uint32_t frameIdx, VkCommandBuffer commandBuffer);
};

#endif // APP_STATE_HPP