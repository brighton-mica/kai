#include <GLFW/glfw3.h>

#include "App.hpp"

constexpr uint32_t WIN_WIDTH_PX = 700;
constexpr uint32_t WIN_HEIGHT_PX = 700;

int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    GLFWwindow* window = glfwCreateWindow(WIN_WIDTH_PX, WIN_HEIGHT_PX, "App", nullptr, nullptr);

    App app { window, WIN_WIDTH_PX, WIN_HEIGHT_PX }; 

    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();

        // Graphics (record and submit draw commands)
        app.render();
    }

    // Wait for all GPU commands to finish executing
    app.cleanup();

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}