#ifndef APP_MODEL
#define APP_MODEL

#include "kai/Buffer.hpp"

#include <cstdint>
#include <array>

namespace shape 
{
    namespace triangle
    {
        static std::array<float, 9> vertices {
            -0.5f,  0.5f, 0.0f, // bot-left
            0.5f,  0.5f, 0.0f, // bot-right
            0.0f, -0.5f, 0.0f  // top-center
        };
    }
}

struct Model
{
    kai::Buffer kVertexBuffer;
    uint32_t vertexCount;

    Model(float* data, const size_t size, const size_t stride)
    {
        const VkBufferCreateInfo createInfo {
            .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
            .size = sizeof(float) * size,
            .usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        };

        kVertexBuffer.create(createInfo, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
        kVertexBuffer.uploadData(createInfo.size, data);

        vertexCount = size / stride;
    }
};

#endif // APP_MODEL