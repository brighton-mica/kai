#ifndef SIMPLE_RENDER_SYSTEM_HPP
#define SIMPLE_RENDER_SYSTEM_HPP

#include "../FrameInfo.hpp"

#include "kai/Device.hpp"
#include "kai/Buffer.hpp"
#include "kai/Pipeline.hpp"

#include "nlohmann/json.hpp"

#include <memory>

// std::array<VertexData, 180> cubeVertices {{
//     { -0.5f, -0.5f, -0.5f,  0.0f, 0.0f },
//     {  0.5f, -0.5f, -0.5f,  1.0f, 0.0f },
//     {  0.5f,  0.5f, -0.5f,  1.0f, 1.0f },
//     {  0.5f,  0.5f, -0.5f,  1.0f, 1.0f },
//     { -0.5f,  0.5f, -0.5f,  0.0f, 1.0f },
//     { -0.5f, -0.5f, -0.5f,  0.0f, 0.0f },

//     { -0.5f, -0.5f,  0.5f,  0.0f, 0.0f },
//     {  0.5f, -0.5f,  0.5f,  1.0f, 0.0f },
//     {  0.5f,  0.5f,  0.5f,  1.0f, 1.0f },
//     {  0.5f,  0.5f,  0.5f,  1.0f, 1.0f },
//     { -0.5f,  0.5f,  0.5f,  0.0f, 1.0f },
//     { -0.5f, -0.5f,  0.5f,  0.0f, 0.0f },

//     { -0.5f,  0.5f,  0.5f,  1.0f, 0.0f },
//     { -0.5f,  0.5f, -0.5f,  1.0f, 1.0f },
//     { -0.5f, -0.5f, -0.5f,  0.0f, 1.0f },
//     { -0.5f, -0.5f, -0.5f,  0.0f, 1.0f },
//     { -0.5f, -0.5f,  0.5f,  0.0f, 0.0f },
//     { -0.5f,  0.5f,  0.5f,  1.0f, 0.0f },

//     {  0.5f,  0.5f,  0.5f,  1.0f, 0.0f },
//     {  0.5f,  0.5f, -0.5f,  1.0f, 1.0f },
//     {  0.5f, -0.5f, -0.5f,  0.0f, 1.0f },
//     {  0.5f, -0.5f, -0.5f,  0.0f, 1.0f },
//     {  0.5f, -0.5f,  0.5f,  0.0f, 0.0f },
//     {  0.5f,  0.5f,  0.5f,  1.0f, 0.0f },

//     { -0.5f, -0.5f, -0.5f,  0.0f, 1.0f },
//     {  0.5f, -0.5f, -0.5f,  1.0f, 1.0f },
//     {  0.5f, -0.5f,  0.5f,  1.0f, 0.0f },
//     {  0.5f, -0.5f,  0.5f,  1.0f, 0.0f },
//     { -0.5f, -0.5f,  0.5f,  0.0f, 0.0f },
//     { -0.5f, -0.5f, -0.5f,  0.0f, 1.0f },

//     { -0.5f,  0.5f, -0.5f,  0.0f, 1.0f },
//     {  0.5f,  0.5f, -0.5f,  1.0f, 1.0f },
//     {  0.5f,  0.5f,  0.5f,  1.0f, 0.0f },
//     {  0.5f,  0.5f,  0.5f,  1.0f, 0.0f },
//     { -0.5f,  0.5f,  0.5f,  0.0f, 0.0f },
//     { -0.5f,  0.5f, -0.5f,  0.0f, 1.0f }
// }};

class Model;

class SimpleRenderSystem
{
private:
    kai::Device& m_kDevice; 
    kai::Pipeline m_kPipeline;

public:
    struct Object
    {
        uint32_t m_uNumVertices;
        kai::Buffer m_kVertexBuffer;

        std::shared_ptr<Model> m_model;

        Object(std::shared_ptr<Model> model);
        void record(VkCommandBuffer commandBuffer) const;
    };

    SimpleRenderSystem(kai::Device& device);

    void createPipeline(const nlohmann::json& json, const VkRenderPass renderPass);
    void record(const FrameInfo& frameInfo, std::vector<SimpleRenderSystem::Object>& renderBin);
};

#endif // SIMPLE_RENDER_SYSTEM_HPP