#include "SimpleRenderSystem.hpp"
#include "../Model.hpp"

#include <vector>

struct VertexData
{
    float pos[3];
};

SimpleRenderSystem::Object::Object(std::shared_ptr<Model> model)
    : m_model { model }
{
}

void SimpleRenderSystem::Object::record(VkCommandBuffer commandBuffer) const
{
    VkDeviceSize offsets[1] { 0u };
    VkBuffer buffer = m_model->kVertexBuffer.getBuffer();
    vkCmdBindVertexBuffers(commandBuffer, 0u, 1u, &buffer, offsets);

    vkCmdDraw(commandBuffer, m_model->vertexCount, 1u, 0u, 0u);
}

SimpleRenderSystem::SimpleRenderSystem(kai::Device& device)
    : m_kDevice { device }
    , m_kPipeline { device }
{}

void SimpleRenderSystem::createPipeline(const nlohmann::json& json, const VkRenderPass renderPass)
{
    kai::PipelineConfig pipelineConfig {};

    m_kPipeline.setupPipeline(json, pipelineConfig);

    //* Override default pipeline here

    // TODO: Reflect vertex input attributes (we can assume only one binding, e.g., that we always interleave)
    // TODO: In the meantime, manually specifiy info here

    const std::vector<VkVertexInputBindingDescription> vertexInputBindingDescription {{
        {
            .binding = 0,
            .stride = sizeof(VertexData),
            .inputRate = VK_VERTEX_INPUT_RATE_VERTEX
        },
    }};

    const std::vector<VkVertexInputAttributeDescription> vertexInputAttributeDescription {{
        {
            .location = 0,
            .binding = 0,
            .format = VK_FORMAT_R32G32B32_SFLOAT,
            .offset = offsetof(VertexData, pos)
        },
    }};

    pipelineConfig.vertexInputBindings = std::move(vertexInputBindingDescription);
    pipelineConfig.vertexInputAttribtutes = std::move(vertexInputAttributeDescription);
    pipelineConfig.renderPass = renderPass;

    m_kPipeline.createPipeline(pipelineConfig);
}

void SimpleRenderSystem::record(const FrameInfo& frameInfo, std::vector<SimpleRenderSystem::Object>& renderBin)
{
    vkCmdBindPipeline(frameInfo.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, m_kPipeline.getPipeline());

    // Bind Global Descriptor Set
    // vkCmdBindDescriptorSets(frameInfo.commandBuffer,
    //     VK_PIPELINE_BIND_POINT_GRAPHICS,
    //     m_kPipeline.getPipelineLayout(),
    //     0u, 1u, &(m_kPipeline.getDescriptorSetLayout(0u)),
    //     0u, nullptr)

    for (const Object& object : renderBin)
    {
        object.record(frameInfo.commandBuffer);
    }
}