#ifndef APP_HELPERS_HPP
#define APP_HELPERS_HPP

#include <vulkan/vulkan.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>

#include <nlohmann/json.hpp>

#include "kai/Defines.hpp"

namespace helpers 
{
    nlohmann::json getJsonFromFile(const std::string& filename)
    {
        // Read base-materials file
        std::ifstream ifs { filename, std::ios::in };
        nlohmann::json file;

        if (ifs.is_open())
        {
            ifs >> file;
            ifs.close();
        }
        else
            EXIT("Error: failed to open file " + filename);

        //! FIXME - copy created upon return
        return file;
    }

    VkShaderModule createShaderModule(VkDevice device, const std::string& filename)
    {
        std::ifstream ifs { "../src/spirv/" + filename, std::ios::in };

        if (ifs.is_open())
        {
            ifs.seekg(0, ifs.end);
            int nbytes = ifs.tellg();
            ifs.seekg(0, ifs.beg);

            std::vector<uint32_t> buffer;
            buffer.resize(nbytes / sizeof(uint32_t));

            ifs.read(reinterpret_cast<char*>(buffer.data()), nbytes);

            ifs.close();

            VkShaderModuleCreateInfo createInfo {};
            createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
            createInfo.codeSize = nbytes;
            createInfo.pCode = buffer.data();

            VkShaderModule shaderModule;
            VK_CHECK(vkCreateShaderModule(device, &createInfo, NULL, &shaderModule));

            return shaderModule;
        }
        else
            EXIT("Failed to open file " + filename);
    }


    VkFormat getVkFormatFromString(const std::string& str)
    {
        const static std::unordered_map<std::string, VkFormat> lookup {
            { "VK_FORMAT_R32G32B32_SFLOAT", VK_FORMAT_R32G32B32_SFLOAT }
        };

        auto iter = lookup.find(str);
        if (iter == lookup.end())
            EXIT("Error: specified format type" + str + " is unknown!");

        return iter->second;
    }

    VkVertexInputRate getVkInputRateFromString(const std::string& str)
    {
        const static std::unordered_map<std::string, VkVertexInputRate> lookup {
            { "VK_VERTEX_INPUT_RATE_VERTEX", VK_VERTEX_INPUT_RATE_VERTEX }
        };

        auto iter = lookup.find(str);
        if (iter == lookup.end())
            EXIT("Error: specified inputRate type" + str + " is unknown!");

        return iter->second;
    }

    VkPrimitiveTopology getVkPrimitiveTopologyFromString(const std::string& str)
    {
        const static std::unordered_map<std::string, VkPrimitiveTopology> lookup {
            { "VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST", VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST }
        };

        auto iter = lookup.find(str);
        if (iter == lookup.end())
            EXIT("Error: specified inputRate type" + str + " is unknown!");

        return iter->second;
    }

    VkPolygonMode getVkPolygonModeFromString(const std::string& str)
    {
    const static std::unordered_map<std::string, VkPolygonMode> lookup {
            { "VK_POLYGON_MODE_FILL", VK_POLYGON_MODE_FILL }
    }; 

        auto iter = lookup.find(str);
        if (iter == lookup.end())
            EXIT("Error: specified inputRate type" + str + " is unknown!");

        return iter->second;
    }
}

#endif // APP_HELPERS_HPP