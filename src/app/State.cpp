#include "State.hpp"
#include "FrameInfo.hpp"
#include "Model.hpp"

#include "kai/Device.hpp"
#include "kai/Swapchain.hpp"
#include "kai/Pipeline.hpp"
#include "kai/Defines.hpp"
#include "Helpers.hpp"

#include <assert.h>
#include <array>
#include <memory>

    // Pre-App
    // -- Collect all instance mat ids from "objects.json" -> instanceIds
    // -- Collect all base mat ids from "material-instances.json" only considering those instances present in instanceIds -> baseIds
    // -- Create reflection data for each unique shader combination in baseIds -> write to "shader-reflection.json"

    // App
    // Iter over base-material list from "base-materials.json"
    // Iter over material-instance list from "material-instances.json"
    // -- if base-material pipeline has not yet been created, create it and add it to baseMaterialRenderBins
    // -- populate material-instance data
    // -- add material-instance to baseMaterialRenderBins[base-material].instanceBins

    // Iter over object list
    // -- create object
    // -- add object to associated material-instance render bin

    // Main Loop
    /*
        ! con - bindPipeline {set{color},blending{false}}, bindMaterialState {name:"red"}
        !     - bindPipeline {set{color},blending{true}},  bindMaterialState {name:"red"}
        ! This sequence triggers a rebind of the instance material state when the state did not change. I think this occurence perf hit
        ! is better than moving pipeline binding below material instance state binding (because pipeline bindinig is slow)


        * force every pipeline layout to share set zero that any subseqeunt pipeline changes/binds will not disturb this set
        bindFrameState()

        * baseMaterialRenderBins: { { set { color }, blending { false } }, { set { color }, blending { true } } }
        for each baseBin in baseMaterialRenderBins

            baseBin.bindPipeline()

            * instanceBins { { name: "red", set { color: (1, 0, 0) } }, { name: "blue", set { color: (0, 0, 1) } } }
            for each instanceBin in baseBin.instanceBins

                instanceBin.bindMaterialState()

                for each object in instanceBin

                    object.bindObjectState()

                    object.draw()
    */

VkDevice BaseMaterial::s_vkDevice = VK_NULL_HANDLE;

struct FrameUbo
{
    float color[3] = { 0.3, 0.3, 0.3 };
};

FrameUbo frameUboData {};



// Iter over "material-reflection.json"
// ?? "material-reflection.json" contains unique shader reflection info for base materials


State::State(GLFWwindow* window, kai::Device& device, kai::Swapchain& swapchain, const uint32_t numBuffers)
    : m_kDevice { device }
    , m_kSwapchain { swapchain }
    , gui { window, device }
    , m_uNumBuffers { numBuffers }
{
    createDepthAttachment();

    createRenderPass();

    createFramebuffers();

    createFrameDescriptorSet();

    createMaterials();

    createScene();

    gui.init(m_kSwapchain.getNumImages(), m_vkRenderPass);
}

State::~State()
{
    baseMaterials.clear();
    renderBins.clear();

    vkDestroyDescriptorPool(m_kDevice.getDevice(), m_vkMaterialDescriptorPool, nullptr);
    vkDestroyDescriptorPool(m_kDevice.getDevice(), m_vkFrameDescriptorPool, nullptr);
    vkDestroyDescriptorSetLayout(m_kDevice.getDevice(), m_vkFrameDescriptorSetLayout, nullptr);

    m_kDepthImage.destroy();
    const uint32_t numImages = m_kSwapchain.getNumImages();
    for (uint32_t i = 0; i < numImages; ++i)
        vkDestroyFramebuffer(m_kDevice.getDevice(), m_vkFramebuffers[i], nullptr);
    
    vkDestroyRenderPass(m_kDevice.getDevice(), m_vkRenderPass, nullptr);
}


void State::createDepthAttachment()
{
    const VkImageCreateInfo depthImageCreateInfo {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .imageType = VK_IMAGE_TYPE_2D,
        .format = VK_FORMAT_D32_SFLOAT,
        .extent = { m_kSwapchain.getExtent().width, m_kSwapchain.getExtent().height, 1u },
        .mipLevels = 1,
        .arrayLayers = 1,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .tiling = VK_IMAGE_TILING_OPTIMAL,
        .usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
    };

    VkImageViewCreateInfo depthImageViewCreateInfo {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = VK_NULL_HANDLE,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = VK_FORMAT_D32_SFLOAT,
        .components = {
            .r = VK_COMPONENT_SWIZZLE_IDENTITY,
            .g = VK_COMPONENT_SWIZZLE_IDENTITY,
            .b = VK_COMPONENT_SWIZZLE_IDENTITY,
            .a = VK_COMPONENT_SWIZZLE_IDENTITY },
        .subresourceRange = {
            .aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1 }
    };

    m_kDepthImage.create(depthImageCreateInfo, depthImageViewCreateInfo);
}

void State::createRenderPass()
{
    const std::array<VkAttachmentDescription, 2> attachments {{
        {   // Color
            .format = m_kSwapchain.getImageFormat(),
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
            .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
        },
        {   // Depth
            .format = VK_FORMAT_D32_SFLOAT,
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
            .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
        }
    }};

    const std::array<VkAttachmentReference, 2> attachmentRefs {{
        {
            .attachment = 0,
            .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
        },
        {
            .attachment = 1,
            .layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
        }
    }};

    const VkSubpassDescription subpass {
        .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
        .inputAttachmentCount = 0,
        .pInputAttachments = nullptr,
        .colorAttachmentCount = 1,
        .pColorAttachments = &(attachmentRefs[0]),
        .pResolveAttachments = nullptr,
        .pDepthStencilAttachment = &(attachmentRefs[1]), 
        .preserveAttachmentCount = 0,
        .pPreserveAttachments = nullptr
    };

    const VkSubpassDependency dependencies[2] {
        {
            // First dependency at the start of the renderpass
            // Does the transition from final to initial layout
            .srcSubpass = VK_SUBPASS_EXTERNAL,                             // Producer of the dependency
            .dstSubpass = 0,                                               // Consumer is our single subpass that will wait for the execution dependency
            // .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // Match our pWaitDstStageMask when we vkQueueSubmit
            .srcStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, // Match our pWaitDstStageMask when we vkQueueSubmit
            .dstStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, // is a loadOp stage for color attachments
            // .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // is a loadOp stage for color attachments
            .srcAccessMask = 0,                                            // semaphore wait already does memory dependency for us
            // .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,         // is a loadOp CLEAR access mask for color attachments
            .dstAccessMask = 0,         // is a loadOp CLEAR access mask for color attachments
            .dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT
        },
        {
            // Second dependency at the end the renderpass
            // Does the transition from the initial to the final layout
            // Technically this is the same as the implicit subpass dependency, but we are gonna state it explicitly here
            .srcSubpass = 0,                                               // Producer of the dependency is our single subpass
            .dstSubpass = VK_SUBPASS_EXTERNAL,                             // Consumer are all commands outside of the renderpass
            .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // is a storeOp stage for color attachments
            .dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,          // Do not block any subsequent work
            .srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,         // is a storeOp `STORE` access mask for color attachments
            .dstAccessMask = 0,
            .dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT
        }
    };
    
    const VkRenderPassCreateInfo renderPassCreateInfo {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .attachmentCount = static_cast<uint32_t>(attachments.size()),
        .pAttachments = attachments.data(),
        .subpassCount = 1,
        .pSubpasses = &subpass,
        .dependencyCount = 2,
        .pDependencies = dependencies
    };

    VK_CHECK(vkCreateRenderPass(
        m_kDevice.getDevice(),
        &renderPassCreateInfo,
        nullptr,
        &m_vkRenderPass));

    // Create RenderPass Begin Info
    static const std::array<VkClearValue, 2> clearValues {{
        {
            .color = { 0.22f, 0.22f, 0.22f, 1.0f }
        },
        {
            .depthStencil = { 1.0f, 0u }
        }
    }};

    m_vkRenderPassBeginInfo = {};
    m_vkRenderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
    m_vkRenderPassBeginInfo.renderPass = m_vkRenderPass,
    m_vkRenderPassBeginInfo.renderArea = {
        .offset = { .x = 0, .y = 0 },
        .extent = m_kSwapchain.getExtent() },
    m_vkRenderPassBeginInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
    m_vkRenderPassBeginInfo.pClearValues = clearValues.data();
}

void State::createFramebuffers()
{
    VkFramebufferCreateInfo framebufferCreateInfo {
        .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
        .renderPass = m_vkRenderPass,
        .attachmentCount = 2,
        .width = m_kSwapchain.getExtent().width,
        .height = m_kSwapchain.getExtent().height,
        .layers = 1
    };

    const uint32_t numImages = m_kSwapchain.getNumImages();
    m_vkFramebuffers.reserve(numImages); 
    
    for (uint32_t i = 0; i < numImages; ++i)
    {
        std::array<VkImageView, 2> attachments = {
            m_kSwapchain.getImageView(i),
            m_kDepthImage.getImageView()
        };

        framebufferCreateInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
        framebufferCreateInfo.pAttachments = attachments.data();

        VK_CHECK(vkCreateFramebuffer(m_kDevice.getDevice(), &framebufferCreateInfo, nullptr, &m_vkFramebuffers[i]));
    }
}

void State::createFrameDescriptorSet()
{
    //! TODO - need to have reflection file with this information as we introduce textures and other dsecriptor types.
    //! need to also split this into per-fram resources to avoid sync issues
    VkDescriptorPoolSize poolSize {};
    poolSize.descriptorCount = 1;
    poolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;

    VkDescriptorPoolCreateInfo poolCreateInfo {};
    poolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolCreateInfo.maxSets = poolSize.descriptorCount;
    poolCreateInfo.poolSizeCount = 1u;
    poolCreateInfo.pPoolSizes = &poolSize;

    VK_CHECK(vkCreateDescriptorPool(m_kDevice.getDevice(), &poolCreateInfo, nullptr, &m_vkFrameDescriptorPool));

    VkDescriptorSetLayoutBinding descBinding {};
    descBinding.binding = 0u;
    descBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descBinding.descriptorCount = 1u;
    descBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

    VkDescriptorSetLayoutCreateInfo layoutCreateInfo {};
    layoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutCreateInfo.bindingCount = 1u;
    layoutCreateInfo.pBindings = &descBinding;

    VK_CHECK(vkCreateDescriptorSetLayout(m_kDevice.getDevice(), &layoutCreateInfo, nullptr, &m_vkFrameDescriptorSetLayout));

    VkDescriptorSetAllocateInfo allocInfo {};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool = m_vkFrameDescriptorPool;
    allocInfo.descriptorSetCount = 1u;
    allocInfo.pSetLayouts = &m_vkFrameDescriptorSetLayout;

    VK_CHECK(vkAllocateDescriptorSets(m_kDevice.getDevice(), &allocInfo, &m_vkFrameDescriptorSet));

    // Create material-instance ubo buffer
    const VkBufferCreateInfo bufferCreateInfo {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = static_cast<uint32_t>(sizeof(FrameUbo)),
        .usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    m_kFrameUbo.create(bufferCreateInfo, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
    m_kFrameUbo.uploadData(bufferCreateInfo.size, static_cast<void*>(&frameUboData));

    // Update descriptors to point to buffer

    VkDescriptorBufferInfo descBufferInfo {}; 
    descBufferInfo.buffer = m_kFrameUbo.getBuffer();
    descBufferInfo.offset = 0u;
    descBufferInfo.range = VK_WHOLE_SIZE; //! TODO - need to fix this when we move to subbuffers

    VkWriteDescriptorSet writeSet {};
    writeSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writeSet.dstSet = m_vkFrameDescriptorSet;
    writeSet.dstBinding = 0;  //! TODO - need to fix this immediately, this will break when more than one binding is present
    writeSet.dstArrayElement = 0;
    writeSet.descriptorCount = 1;
    writeSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    writeSet.pBufferInfo = &descBufferInfo;

    vkUpdateDescriptorSets(m_kDevice.getDevice(), 1u, &writeSet, 0u, nullptr);
}

void State::createMaterials()
{
    // External deps
    // VkDevice device;
    // VkExtent2D swapchainExtent;
    // VkRenderPass renderPass;

    BaseMaterial::s_vkDevice = m_kDevice.getDevice();

    VkGraphicsPipelineCreateInfo pipelineCreateInfo { VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO };
    
    // The material defines the pipeline, well, most of it. There is some global state which is shared amongst all materials/pipelies.
    // Define and set the global state once now, rather than per-used base-material in the loop

    const VkViewport viewport {
        .x = 0,
        .y = 0,
        .width = static_cast<float>(m_kSwapchain.getExtent().width),
        .height = static_cast<float>(m_kSwapchain.getExtent().height),
        .minDepth = 0.0f,
        .maxDepth = 1.0f,
    };

    const VkRect2D scissorRect {
        .offset = {.x = 0, .y = 0},
        .extent = m_kSwapchain.getExtent(),
    };

    const VkPipelineViewportStateCreateInfo viewportState {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .viewportCount = 1u,
        .pViewports = &viewport,
        .scissorCount = 1u,
        .pScissors = &scissorRect
    };

    const VkPipelineMultisampleStateCreateInfo multisampleState {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
        .sampleShadingEnable = VK_FALSE,
        .minSampleShading = 0,
        .pSampleMask = nullptr,
        .alphaToCoverageEnable = VK_FALSE,
        .alphaToOneEnable = VK_FALSE,
    };

    const VkPipelineDepthStencilStateCreateInfo depthStencilState {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .depthTestEnable = VK_TRUE,
        .depthWriteEnable = VK_TRUE,
        .depthCompareOp = VK_COMPARE_OP_LESS,
        .depthBoundsTestEnable = VK_FALSE,
        .stencilTestEnable = VK_FALSE,
        .minDepthBounds = 0.0f,
        .maxDepthBounds = 1.0f
    };

    pipelineCreateInfo.pViewportState = &viewportState;
    pipelineCreateInfo.pMultisampleState = &multisampleState;
    pipelineCreateInfo.pDepthStencilState = &depthStencilState;
    pipelineCreateInfo.pDynamicState = nullptr;
    pipelineCreateInfo.renderPass = m_vkRenderPass;
    pipelineCreateInfo.subpass = 0;

    // Here onwards, we are processing material reflection files and creating pipelines/materials based on 
    // base-material states.

    // Read config files
    const std::string baseMaterialFilePath  = "../test-config/app-state/reflection-base-materials.json";
    const std::string shaderSetFilePath = "../test-config/app-state/reflection-base-material-shader-sets.json";
    const std::string materialInstancesFilePath = "../test-config/material-instances.json";

    nlohmann::json baseMaterialsJson = helpers::getJsonFromFile(baseMaterialFilePath);
    nlohmann::json shaderSetsJson = helpers::getJsonFromFile(shaderSetFilePath);
    nlohmann::json materialInstancesJson = helpers::getJsonFromFile(materialInstancesFilePath);

    // Before we begin creating materials, we need to enable the allocation and assignment of descriptor sets to newly
    // created materials. To do this, we create a descriptor pool now. This descriptor pool is only responsible for
    // per-material descriptor bindings.
    //! TODO - need to have reflection file with this information as we introduce textures and other dsecriptor types.
    VkDescriptorPoolSize poolSize {};
    poolSize.descriptorCount = materialInstancesJson["material-instances"].size();
    poolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;

    VkDescriptorPoolCreateInfo poolCreateInfo {};
    poolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolCreateInfo.maxSets = poolSize.descriptorCount;
    poolCreateInfo.poolSizeCount = 1u;
    poolCreateInfo.pPoolSizes = &poolSize;

    VK_CHECK(vkCreateDescriptorPool(m_kDevice.getDevice(), &poolCreateInfo, nullptr, &m_vkMaterialDescriptorPool));

    // Set pipeline properties unique to each loaded material then create the pipeline
    for (auto& baseMatJson : baseMaterialsJson["base-material-reflections"])
    {
        const uint32_t baseMatId = baseMatJson["id"].get<uint32_t>();
        const uint32_t shaderSetIdx = baseMatJson["shader-set-idx"].get<uint32_t>();
        nlohmann::json& shaderSet = shaderSetsJson["base-material-shader-sets"][shaderSetIdx];

        auto baseMatIter = baseMaterials.find(baseMatId);
        if (baseMatIter != baseMaterials.end())
            EXIT("Error: attempint to process a base-material with a non-unique id!");
        
        baseMaterials[baseMatId];
        BaseMaterial& newBaseMat = baseMaterials[baseMatId];
        newBaseMat.id = baseMatJson["id"].get<uint32_t>();
        newBaseMat.uboSize = shaderSet["ubo"]["size"].get<uint32_t>();

        // Shader stages

        nlohmann::json& shadersJson = shaderSet["shaders"];
        const uint32_t shaderCount = shadersJson.size();
        std::vector<VkPipelineShaderStageCreateInfo> shaderStages (shaderCount);

        for (size_t i = 0; i < shaderCount; ++i)
        {
            shaderStages[i].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
            shaderStages[i].stage = shadersJson[i]["stage"];
            shaderStages[i].module = helpers::createShaderModule(m_kDevice.getDevice(), shadersJson[i]["name"]);
            std::string copy = shadersJson[i]["entryPoint"].get<std::string>().c_str();
            shaderStages[i].pName = copy.c_str();
        }

        // Vertex inputs

        nlohmann::json& inputAttribJson = baseMatJson["input-attributes"];
        const uint32_t inputAttribCount = inputAttribJson.size();
        std::vector<VkVertexInputAttributeDescription> inputAttribDescs (inputAttribCount);

        for (size_t i = 0; i < inputAttribCount; ++i)
        {
            inputAttribDescs[i].location = inputAttribJson[i]["location"];
            inputAttribDescs[i].binding = inputAttribJson[i]["binding"];
            inputAttribDescs[i].format = helpers::getVkFormatFromString(inputAttribJson[i]["format"]);
            inputAttribDescs[i].offset = inputAttribJson[i]["offset"];
        }

        nlohmann::json& inputBindingJson = baseMatJson["input-bindings"];
        const uint32_t inputBindingCount = inputBindingJson.size();
        std::vector<VkVertexInputBindingDescription> inputBindingDescs (inputBindingCount);

        for (size_t i = 0; i < inputBindingCount; ++i)
        {
            inputBindingDescs[i].binding = inputBindingJson[i]["id"];
            inputBindingDescs[i].stride = inputBindingJson[i]["stride"];
            inputBindingDescs[i].inputRate = helpers::getVkInputRateFromString(inputBindingJson[i]["inputRate"]);
        }

        const VkPipelineVertexInputStateCreateInfo vertexInputState {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
            .vertexBindingDescriptionCount = inputBindingCount,
            .pVertexBindingDescriptions = inputBindingDescs.data(),
            .vertexAttributeDescriptionCount = inputAttribCount,
            .pVertexAttributeDescriptions = inputAttribDescs.data()
        };

        // Input assembly

        const VkPipelineInputAssemblyStateCreateInfo inputAssemblyState {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
            .topology = helpers::getVkPrimitiveTopologyFromString(baseMatJson["topology"]),
            .primitiveRestartEnable = VK_FALSE
        };

        // Rasterization

        const VkPipelineRasterizationStateCreateInfo rasterizationState {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
            .depthClampEnable = VK_FALSE,
            .rasterizerDiscardEnable = VK_FALSE,
            .polygonMode = helpers::getVkPolygonModeFromString(baseMatJson["polygon-mode"]), 
            .cullMode = VK_CULL_MODE_NONE,
            .frontFace = VK_FRONT_FACE_CLOCKWISE,
            .depthBiasEnable = VK_FALSE,
            .depthBiasConstantFactor = 0.f,
            .depthBiasClamp = 0.f,
            .depthBiasSlopeFactor = 0.f,
            .lineWidth = 1.f,
        };

        // Blend state

        VkPipelineColorBlendAttachmentState colorBlendAttachmentState {};
        VkPipelineColorBlendStateCreateInfo colorBlendState {};

        nlohmann::json& blendStateJson = baseMatJson["blend-state"];
        if (blendStateJson["enabled"].get<bool>() == true)
        {
            EXIT("Error: we do not yet support blended materials!");
        }
        else
        {
            colorBlendAttachmentState.blendEnable = VK_FALSE;
            colorBlendAttachmentState.srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
            colorBlendAttachmentState.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
            colorBlendAttachmentState.colorBlendOp = VK_BLEND_OP_ADD;
            colorBlendAttachmentState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
            colorBlendAttachmentState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
            colorBlendAttachmentState.alphaBlendOp = VK_BLEND_OP_ADD;
            colorBlendAttachmentState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
            
            colorBlendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
            colorBlendState.logicOpEnable = VK_FALSE;
            colorBlendState.logicOp = VK_LOGIC_OP_COPY;
            colorBlendState.attachmentCount = 1;
            colorBlendState.pAttachments = &colorBlendAttachmentState;
            colorBlendState.blendConstants[0] = 0.f;
            colorBlendState.blendConstants[1] = 0.f;
            colorBlendState.blendConstants[2] = 0.f;
            colorBlendState.blendConstants[3] = 0.f;
        }

        // Descriptor set layouts

        newBaseMat.m_vkDescriptorSetLayouts.reserve(shaderSet["descSetLayouts"].size());

        for (const auto& layoutInfo : shaderSet["descSetLayouts"])
        {
            std::vector<VkDescriptorSetLayoutBinding> descSetLayoutBindings;

            for (const auto& bindingInfo : layoutInfo)
            {
                VkDescriptorSetLayoutBinding layoutBinding {};
                layoutBinding.binding = bindingInfo["binding"].get<uint32_t>();
                layoutBinding.descriptorType = bindingInfo["descriptorType"].get<VkDescriptorType>();
                layoutBinding.descriptorCount = bindingInfo["descriptorCount"].get<uint32_t>();
                layoutBinding.stageFlags = bindingInfo["stageFlags"].get<VkShaderStageFlags>();

                descSetLayoutBindings.push_back(std::move(layoutBinding));
            }

            VkDescriptorSetLayoutCreateInfo createInfo {};
            createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
            createInfo.bindingCount = static_cast<uint32_t>(descSetLayoutBindings.size()); 
            createInfo.pBindings = descSetLayoutBindings.data();

            VkDescriptorSetLayout layout;
            VK_CHECK(vkCreateDescriptorSetLayout(m_kDevice.getDevice(), &createInfo, nullptr, &layout));

            newBaseMat.m_vkDescriptorSetLayouts.push_back( layout );
        }

        // Pipeline layout

        VkPipelineLayoutCreateInfo layoutCreateInfo {};
        layoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        layoutCreateInfo.setLayoutCount = static_cast<uint32_t>(newBaseMat.m_vkDescriptorSetLayouts.size());
        layoutCreateInfo.pSetLayouts = newBaseMat.m_vkDescriptorSetLayouts.data();
        layoutCreateInfo.pushConstantRangeCount = 0u;
        layoutCreateInfo.pPushConstantRanges = nullptr;

        VK_CHECK(vkCreatePipelineLayout(m_kDevice.getDevice(), &layoutCreateInfo, nullptr, &newBaseMat.m_vkPipelineLayout));

        // Material state
        pipelineCreateInfo.stageCount = shaderCount;
        pipelineCreateInfo.pStages = shaderStages.data();
        pipelineCreateInfo.pVertexInputState = &vertexInputState;
        pipelineCreateInfo.pInputAssemblyState = &inputAssemblyState;
        pipelineCreateInfo.pRasterizationState = &rasterizationState;
        pipelineCreateInfo.pColorBlendState = &colorBlendState;
        pipelineCreateInfo.layout = newBaseMat.m_vkPipelineLayout;

        VK_CHECK(vkCreateGraphicsPipelines(m_kDevice.getDevice(), VK_NULL_HANDLE, 1u, &pipelineCreateInfo, nullptr, &newBaseMat.m_vkPipeline));

        for (size_t i = 0; i < shaderStages.size(); ++i)
            vkDestroyShaderModule(m_kDevice.getDevice(), shaderStages[i].module, nullptr);

        // All material-instances for this base-material need ubo member info to properly populate their ubo. Populate
        // base-material ubo member (of which all material-instances will refer to) now.
        for (auto& [key, value] : shaderSet["ubo"]["members"].items())
        {
            auto iter = newBaseMat.uboMemberInfos.find(key);
            if (iter != newBaseMat.uboMemberInfos.end())
                EXIT("Error: same member name " + key + " specified multilple times in base-matierial UBO!");

            newBaseMat.uboMemberInfos[key] = { value["offset"].get<uint32_t>(), value["size"].get<uint32_t>() };
        }

        // All material-instances of this base-material need a descriptor set in order to bind material data. 
        // Allocate "n" descriptor sets now. - where "n" refers to the number of material-instances which
        // reference this base-material.
        const uint32_t numMatInstRefs = baseMatJson["material-instance-ref-count"].get<uint32_t>();
        std::vector<VkDescriptorSetLayout> tmpLayouts (numMatInstRefs, newBaseMat.m_vkDescriptorSetLayouts[1]);
        newBaseMat.m_vkDescriptorSets.reserve(numMatInstRefs);

        VkDescriptorSetAllocateInfo allocInfo {};
        allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        allocInfo.descriptorPool = m_vkMaterialDescriptorPool;
        allocInfo.descriptorSetCount = numMatInstRefs;
        allocInfo.pSetLayouts = tmpLayouts.data();

        VK_CHECK(vkAllocateDescriptorSets(m_kDevice.getDevice(), &allocInfo, newBaseMat.m_vkDescriptorSets.data()));
    }

    // Now that all requested base-materials have been created and added to State, we process
    // the material-instances-reflection file. All material-instances will be added to its
    // associated base-material structure (each with its unique set of descriptor data).

    std::unordered_map<uint32_t, uint32_t> descSetIdx;

    for (nlohmann::json& matInstJson : materialInstancesJson["material-instances"])
    {
        const uint32_t baseMatIdx = matInstJson["base-material-id"].get<uint32_t>();
        auto baseMatIter = baseMaterials.find(baseMatIdx);
        if (baseMatIter == baseMaterials.end())
            EXIT("Error: material instance references base-material but base-material hasn't been created yet. This should never happen!");

        // Add material-instance to associated base-material
        const uint32_t newMatInstId = matInstJson["id"].get<uint32_t>(); 
        auto matInstIter = baseMatIter->second.materialInstances.find(newMatInstId);
        if (matInstIter != baseMatIter->second.materialInstances.end())
            EXIT("Error: material-instance referenced multiple times by one base-material");

        baseMatIter->second.materialInstances[newMatInstId];
        BaseMaterial::MaterialInstance& newMatInst = baseMatIter->second.materialInstances[newMatInstId];
            
        newMatInst.id = newMatInstId;
        newMatInst.name = matInstJson["name"].get<std::string>();
        newMatInst.uboData.resize(baseMatIter->second.uboSize);

        // Populate material-instance ubo raw data
        const auto& baseMatUboMemberInfos = baseMatIter->second.uboMemberInfos;

        for (auto& [key, value] : matInstJson["ubo"].items())
        {
            const BaseMaterial::UboMemberInfo& memberInfo = baseMatUboMemberInfos.at(key);

            std::vector<float> vec;
            vec.reserve(memberInfo.size / sizeof(float));
            value.get_to(vec);

            memcpy(newMatInst.uboData.data() + memberInfo.offset, static_cast<void*>(vec.data()), memberInfo.size);
        }

        // Create material-instance ubo buffer
        const VkBufferCreateInfo bufferCreateInfo {
            .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
            .size = static_cast<uint32_t>(newMatInst.uboData.size()),
            .usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
            .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        };

        newMatInst.buffer.create(bufferCreateInfo, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
        newMatInst.buffer.uploadData(bufferCreateInfo.size, static_cast<void*>(newMatInst.uboData.data()));

        // Set descriptor set
        newMatInst.descriptorSet = baseMatIter->second.m_vkDescriptorSets[(descSetIdx[baseMatIdx]++)];

        VkDescriptorBufferInfo descBufferInfo {}; 
        descBufferInfo.buffer = newMatInst.buffer.getBuffer();
        descBufferInfo.offset = 0u;
        descBufferInfo.range = VK_WHOLE_SIZE; //! TODO - need to fix this when we move to subbuffers

        VkWriteDescriptorSet writeSet {};
        writeSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writeSet.dstSet = newMatInst.descriptorSet;
        writeSet.dstBinding = 0;  //! TODO - need to fix this immediately, this will break when more than one binding is present
        writeSet.dstArrayElement = 0;
        writeSet.descriptorCount = 1;
        writeSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        writeSet.pBufferInfo = &descBufferInfo;

        // ! TODO - need to batch this, currently did not due to desc buff info ref
        vkUpdateDescriptorSets(m_kDevice.getDevice(), 1u, &writeSet, 0u, nullptr);
    }

    std::cout << "Done Creating Materials\n";
}

void State::createScene()
{
    const std::string objectsFilePath = "../test-config/app-state/reflection-objects.json";

    nlohmann::json objectsJson = helpers::getJsonFromFile(objectsFilePath);

    for (nlohmann::json& objectJson : objectsJson["objects"])
    {
        Renderable newRenderable {};

        newRenderable.name = objectJson["name"].get<std::string>();
        newRenderable.materialInstanceId = objectJson["material-instance-id"].get<uint32_t>();
        newRenderable.baseMaterialId = objectJson["base-material-id"].get<uint32_t>();

        // Create vertex buffer
        //! TODO - put vertices in separate file to read
        std::vector<int> vertexData = objectJson["vertices"];

        // Create material-instance ubo buffer
        const VkBufferCreateInfo bufferCreateInfo {
            .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
            .size = static_cast<uint32_t>(sizeof(float) * vertexData.size()),
            .usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        };

        newRenderable.vertexBuffer.create(bufferCreateInfo, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
        newRenderable.vertexBuffer.uploadData(bufferCreateInfo.size, vertexData.data());

        //! TODO - create ubo

        // Add to appropriate render bin
        renderBins[newRenderable.baseMaterialId][newRenderable.materialInstanceId].push_back( std::move(newRenderable) );
    }
}

void State::executeFrame(const uint32_t frameIdx, VkCommandBuffer commandBuffer)
{
    m_vkRenderPassBeginInfo.framebuffer = m_vkFramebuffers[m_kSwapchain.getImageIdx()];
    vkCmdBeginRenderPass(commandBuffer, &m_vkRenderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

    // bind frame state (held state holds this)
    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, baseMaterials[0].m_vkPipeline);

    // Bind frame set

    for (auto it = renderBins.begin(); it != renderBins.end(); ++it)
    {


    }

/*
        * force every pipeline layout to share set zero that any subseqeunt pipeline changes/binds will not disturb this set
        bindFrameState()

        * baseMaterialRenderBins: { { set { color }, blending { false } }, { set { color }, blending { true } } }
        for each baseBin in baseMaterialRenderBins

            baseBin.bindPipeline()

            * instanceBins { { name: "red", set { color: (1, 0, 0) } }, { name: "blue", set { color: (0, 0, 1) } } }
            for each instanceBin in baseBin.instanceBins

                instanceBin.bindMaterialState()

                for each object in instanceBin

                    object.bindObjectState()

                    object.draw()
    */


    

    // m_simpleRenderSystem.record(frameInfo, m_simpleRenderSystemBin);

    gui.record(commandBuffer);

    vkCmdEndRenderPass(commandBuffer);
}


