#ifndef KAI_CAMERA_HPP
#define KAI_CAMERA_HPP

#include <iostream>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

#include <GLFW/glfw3.h>

class Camera
{
private:

    static glm::mat4 m_viewMatrix;
    static glm::vec3 m_upVector;
    static glm::vec3 m_eyePos;
    static glm::vec3 m_lookAt;

    static bool m_bMouseDown;

    static glm::vec2 m_windowExtent;
    static glm::vec2 m_mousePos;

    static glm::vec3 getViewDirection()
    {
        return glm::transpose(m_viewMatrix)[2];
    }

    static glm::vec3 getRightVector()
    {
        return glm::transpose(m_viewMatrix)[0];
    }

public:
    Camera(const uint32_t windowWidth, const uint32_t windowHeight, const glm::vec3 eyePos, const glm::vec3 point)
    {
        m_bMouseDown = false;
        m_windowExtent.x = windowWidth;
        m_windowExtent.y = windowHeight;
        m_eyePos = eyePos;
        m_lookAt = point;

        updateViewMatrix();
    }

    static void cursorPosCB(GLFWwindow* window, double xpos, double ypos)
    {
        if (m_bMouseDown)
        {
            const double xAngle = (m_mousePos.x - xpos) * (2 * glm::pi<double>() / m_windowExtent.x);
            double yAngle = (m_mousePos.y - ypos) * (glm::pi<double>() / m_windowExtent.y);

            // Extra step to handle the problem when the camera direction is the same as the up vector
            float cosAngle = glm::dot(getViewDirection(), m_upVector);
            if (cosAngle * glm::sign(yAngle) > 0.99f)
                yAngle = 0;

            glm::vec4 pos { m_eyePos.x, m_eyePos.y, m_eyePos.z, 1.f };
            glm::vec4 pivot { m_lookAt.x, m_lookAt.y, m_lookAt.z, 1.f };

            // step 2: Rotate the camera around the pivot point on the first axis.
            glm::mat4x4 rotationMatrixX { 1.0f };
            rotationMatrixX = glm::rotate(rotationMatrixX, static_cast<float>(xAngle), m_upVector);
            pos = (rotationMatrixX * (pos - pivot)) + pivot;

            // step 3: Rotate the camera around the pivot point on the second axis.
            glm::mat4x4 rotationMatrixY(1.0f);
            rotationMatrixY = glm::rotate(rotationMatrixY, static_cast<float>(yAngle), getRightVector());
            glm::vec3 finalPosition = (rotationMatrixY * (pos - pivot)) + pivot;

            m_eyePos = finalPosition;
            updateViewMatrix();

            m_mousePos.x = xpos;
            m_mousePos.y = ypos;
        }
    }

    static void mouseButtonCB(GLFWwindow* window, int button, int action, int mods)
    {
        if (button = GLFW_MOUSE_BUTTON_RIGHT)
        {
            if (action == GLFW_PRESS)
            {
                m_bMouseDown = true;
            }
            else if (action == GLFW_RELEASE)
            {
                m_bMouseDown = false;
            }
        }
    }

    static void scrollCB(GLFWwindow* window, double xoffset, double yoffset)
    {
        std::cout << "scroll\n";
    }

    static void updateViewMatrix()
    {
        m_viewMatrix = glm::lookAt(m_eyePos, m_lookAt, m_upVector);
        for (int i = 0; i < 4; ++i)
        {
            std::cout << "col " << i << ": ";
            for (int j = 0; j < 4; j++)
            {
                std::cout << m_viewMatrix[i][j]  << " ";
            }
            std::cout << "\n";
        }
    }
};

bool Camera::m_bMouseDown = false;
glm::vec2 Camera::m_windowExtent {};
glm::vec2 Camera::m_mousePos {};
glm::mat4 Camera::m_viewMatrix {};
glm::vec3 Camera::m_upVector { 0.0, 1.0, 0.0 };
glm::vec3 Camera::m_eyePos {};
glm::vec3 Camera::m_lookAt {};

#endif // KAI_CAMERA_HPP