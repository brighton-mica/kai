#include "App.hpp"

App::App(GLFWwindow* window, const uint32_t windowWidth, const uint32_t windowHeight)
    : m_kDevice { window }
    , m_kSwapchain { m_kDevice, windowWidth, windowHeight, NUM_BUFFERS }
    , m_kFrames { { }, { } }
    , m_uFrameIdx { 0 }
    , m_kState { window, m_kDevice, m_kSwapchain, NUM_BUFFERS }
    // , m_kCamera { windowWidth, windowHeight, { 0.0, 0.0, 5.0 }, { 0.0, 0.0, 0.0 } }
{ 
    // glfwSetCursorPosCallback(window, m_kCamera.cursorPosCB);
    // glfwSetMouseButtonCallback(window, m_kCamera.mouseButtonCB);
    // glfwSetScrollCallback(window, m_kCamera.scrollCB);
}

void App::render()
{
    kai::Frame& frame = m_kFrames[m_uFrameIdx];

    frame.preRender();

    m_kState.executeFrame(m_uFrameIdx, frame.getCommandBuffer());

    frame.postRender();

    m_uFrameIdx = (m_uFrameIdx + 1) % NUM_BUFFERS;
}

void App::cleanup()
{
    m_kDevice.waitIdle();
}