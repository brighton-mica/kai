#ifndef FRAME_INFO
#define FRAME_INFO

#include <vulkan/vulkan.h>

struct FrameInfo
{ 
    uint32_t frameIdx;
    VkCommandBuffer commandBuffer;
};

#endif // FRAME_INFO
