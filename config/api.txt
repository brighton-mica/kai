
usedModelNamesSet = []
for objectJSON : objects:
    usedModelNamesSet.append(objectJSON["modelName"])

for modelName : usedModelNamesSet:
    // write model to used models json



// App

map<string, shared_ptr<Model>> modelLookupTable {}

for model : modelsJSON["models"]:
    if model["name"] not in modelLookupTable:
        modelLookupTable[model["name"]] = new Model( 
            model["vertexInfo"]["bufferData"],
            model["vertexInfo"]["bufferSize"],
            model["vertexInfo"]["vertexCount"]);

SimpleRenderSystemObject object;
object.setDescSet(pipeline.getSetLayout(eDescSetLayoutDraw));
object.setModel(modelLookupTable["triangle"]);

// -- Object setDescSet

// -- SimpleRenderSystem record

for (Object object : objects):
    object.bindDescSet()          // need to create this guy somewhere
    object.model->bindBuffer()
    object.model->record()






